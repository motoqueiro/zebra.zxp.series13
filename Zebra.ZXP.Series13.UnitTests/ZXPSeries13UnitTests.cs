﻿using System;
using System.Drawing.Printing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zebra.ZXP.Series13.Enums;

namespace Zebra.ZXP.Series13.UnitTests
{
    [TestClass]
    public class ZXPSeries13UnitTests
    {
        private ZXPSeries13 _zebraPrinter;

        public ZXPSeries13UnitTests()
        {
            var printerName = GetZebraPrinterName();
            _zebraPrinter = new ZXPSeries13(printerName);
        }

        ~ZXPSeries13UnitTests()
        {
            if (_zebraPrinter != null)
                _zebraPrinter.Dispose();
        }

        private string GetZebraPrinterName()
        {
            foreach (string printerName in PrinterSettings.InstalledPrinters)
            {
                if (printerName.Contains("ZXP Series 1") || printerName.Contains("ZXP Series 3"))
                    return printerName;
            }
            return null;
        }

        [TestMethod]
        public void ConstructorTest()
        {
            Assert.IsNotNull(_zebraPrinter.Handle);
            Assert.AreEqual(_zebraPrinter.PrinterType, PrinterTypeEnum.SingleSided);
        }

        #region SDK Specific Tests

        [TestMethod]
        [TestCategory("SDK Specific Tests")]
        public void GetSDKVerTest()
        {
            int major, minor, engineeringLevel;
            _zebraPrinter.GetSDKVer(out major, out minor, out engineeringLevel);
            Assert.AreNotEqual(major, 0);
            Assert.AreNotEqual(minor, 0);
            Assert.AreNotEqual(engineeringLevel, 0);
            Console.WriteLine("SDK Version -> Major: {0} | Minor: {1} | EngineeringLevel: {2}", major, minor, engineeringLevel);
        }

        [TestMethod]
        [TestCategory("SDK Specific Tests")]
        public void GetSDKVsnTest()
        {
            int major, minor, engineeringLevel;
            _zebraPrinter.GetSDKVsn(out major, out minor, out engineeringLevel);
            Assert.AreNotEqual(major, 0);
            Assert.AreNotEqual(minor, 0);
            Assert.AreNotEqual(engineeringLevel, 0);
            Console.WriteLine("SDK Version -> Major: {0} | Minor: {1} | EngineeringLevel: {2}", major, minor, engineeringLevel);
        }

        [Ignore]
        [TestMethod]
        [TestCategory("SDK Specific Tests")]
        public void GetSDKProductVerTest()
        {
            var productVersion = _zebraPrinter.GetSDKProductVer();
            UnitTestHelpers.AssertIsNotNullOrEmptyString(productVersion);
        }

        #endregion SDK Specific Tests

        #region Printer Driver Tests Handle Tests

        [TestMethod]
        [TestCategory("Printer Driver Tests Handle Tests")]
        public void GetHandleTest()
        {
            _zebraPrinter.GetHandle();
        }

        [TestMethod]
        [TestCategory("Printer Driver Tests Handle Tests")]
        public void CloseHandleTest()
        {
            _zebraPrinter.CloseHandle();
        }

        #endregion Printer Driver Tests Handle Tests

        #region Printer Driver Tests

        [TestMethod]
        [TestCategory("Printer Driver Tests")]
        public void GetMessageSupressionStateTest()
        {
            var supressionState = _zebraPrinter.GetMessageSupressionState();
            Assert.AreNotEqual(supressionState, MessageSupressionStateEnum.Unknown);
            Console.WriteLine("MessageSupressionState: {0}", supressionState.ToString());
        }

        [TestMethod]
        [TestCategory("Printer Driver Tests")]
        public void UnSupressStatusMessagesTest()
        {
            Assert.IsTrue(_zebraPrinter.SupressStatusMessages(MessageSupressionStateEnum.SuppressionDisabled));
        }

        [TestMethod]
        [TestCategory("Printer Driver Tests")]
        public void SupressStatusMessagesTest()
        {
            Assert.IsTrue(_zebraPrinter.SupressStatusMessages(MessageSupressionStateEnum.SuppressionEnabled));
        }

        [TestMethod]
        [TestCategory("Printer Driver Tests")]
        public void SetOverlayType()
        {
            Assert.IsTrue(_zebraPrinter.SetOverlayType(OverlaySideEnum.Back, OverlayTypeEnum.NO_OVERLAY, "overlay_image.jpg"));
        }

        #endregion Printer Driver Tests

        #region Print Command Tests

        [TestMethod]
        [TestCategory("Printer Command Tests")]
        public void PrintPrnFileTest()
        {
            Assert.IsTrue(_zebraPrinter.PrintPrnFile("sample.prn"));
        }

        #endregion Print Command Tests

        #region Status Tests

        [TestMethod]
        [TestCategory("Status Tests")]
        public void CheckForErrorsTest()
        {
            var errorCode = _zebraPrinter.CheckForErrors();
            if (errorCode != null)
                UnitTestHelpers.AssertErrorCode(errorCode);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void ClearErrorStatusLineTest()
        {
            Assert.IsTrue(_zebraPrinter.ClearErrorStatusLine());
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void GetPrintCountTest()
        {
            var count = _zebraPrinter.GetPrintCount();
            Assert.IsTrue(count > 0);
            Console.WriteLine("PrintCount: {0}", count);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void GetPrintStatusTest()
        {
            var errorCode = _zebraPrinter.GetPrintStatus();
            if (errorCode != null)
                UnitTestHelpers.AssertErrorCode(errorCode);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void GetPanelsRemainingTest()
        {
            var panelsRemaining = _zebraPrinter.GetPanelsRemaining();
            Assert.IsTrue(panelsRemaining > 0);
            Console.WriteLine("PanelsRemaining: {0}", panelsRemaining);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void GetPanelsPrintedTest()
        {
            var panelsPrinted = _zebraPrinter.GetPanelsPrinted();
            Assert.IsTrue(panelsPrinted > 0);
            Console.WriteLine("PanelsPrinted: {0}", panelsPrinted);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void GetPrinterSerialNumberTest()
        {
            var serialNumber = _zebraPrinter.GetPrinterSerialNumber();
            UnitTestHelpers.AssertIsNotNullOrEmptyString(serialNumber);
            Console.WriteLine("PrinterSerialNumber: {0}", serialNumber);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void GetPrinterSerialNumbTest()
        {
            var serialNumber = _zebraPrinter.GetPrinterSerialNumb();
            UnitTestHelpers.AssertIsNotNullOrEmptyString(serialNumber);
            Console.WriteLine("PrinterSerialNumber: {0}", serialNumber);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void GetPrinterOptionsTest()
        {
            var printerOptions = _zebraPrinter.GetPrinterOptions();
            UnitTestHelpers.AssertPrinterOptions(printerOptions);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void GetPrintHeadSerialNumberTest()
        {
            var serialNumber = _zebraPrinter.GetPrintHeadSerialNumber();
            UnitTestHelpers.AssertIsNotNullOrEmptyString(serialNumber);
            Console.WriteLine("PrinterHeadSerialNumber: {0}", serialNumber);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void GetPrintHeadSerialNumbTest()
        {
            var serialNumber = _zebraPrinter.GetPrintHeadSerialNumb();
            UnitTestHelpers.AssertIsNotNullOrEmptyString(serialNumber);
            Console.WriteLine("PrinterHeadSerialNumber: {0}", serialNumber);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void GetOperationalParameterTest()
        {
            var value = _zebraPrinter.GetOperationalParameter(OperationalParameterEnum.BlackContrast);
            UnitTestHelpers.AssertIsNotNullOrEmptyString(value);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void GetOperationalParametersTest()
        {
            var operationalParameters = _zebraPrinter.GetOperationalParameters();
            UnitTestHelpers.AssertOperationalParameters(operationalParameters);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void GetPrinterStatusTest()
        {
            var errorCode = _zebraPrinter.GetPrinterStatus();
            if (errorCode != null)
                UnitTestHelpers.AssertErrorCode(errorCode);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void GetSensorStatusTest()
        {
            var sensorStatus = _zebraPrinter.GetSensorStatus();
            Assert.IsNotNull(sensorStatus);
            UnitTestHelpers.AssertSensorStatus(sensorStatus);
        }

        [TestMethod]
        [TestCategory("Status Tests")]
        public void IsPrinterReadyTest()
        {
            Assert.IsTrue(_zebraPrinter.IsPrinterReady());
        }

        #endregion Status Tests

        #region Cleaning Tests

        [TestMethod]
        [TestCategory("Cleaning Tests")]
        public void CheckDueForCleaningTest()
        {
            int printingCounter, cleaningCounter, cleaningCardCounter;
            var result = _zebraPrinter.CheckDueForCleaning(out printingCounter, out cleaningCounter, out cleaningCardCounter);
            Assert.IsTrue(result);
            Assert.AreNotEqual(0, printingCounter);
            Assert.AreNotEqual(0, cleaningCounter);
            Assert.AreNotEqual(0, cleaningCardCounter);
            Console.WriteLine("Due for Cleaning Tests -> PrintingCounter: {0} | CleaningCounter: {1} | CleaningCardCounter: {2}", printingCounter, cleaningCounter, cleaningCardCounter);
        }

        [TestMethod]
        [TestCategory("Cleaning Tests")]
        public void StartCleaningSequenceTest()
        {
            Assert.IsTrue(_zebraPrinter.StartCleaningSequence());
        }

        [TestMethod]
        [TestCategory("Cleaning Tests")]
        public void GetCleaningParametersTest()
        {
            int printingCounter, cleaningCounter, cleaningCardCounter;
            var result = _zebraPrinter.GetCleaningParameters(out printingCounter, out cleaningCounter, out cleaningCardCounter);
            Assert.IsTrue(result);
            Assert.AreNotEqual(printingCounter, 0);
            Assert.AreNotEqual(cleaningCounter, 0);
            Assert.AreNotEqual(cleaningCardCounter, 0);
            Console.WriteLine("Cleaning Tests Parameters -> PrintingCounter: {0} | CleaningCounter: {1} | CleaningCardCounter: {2}", printingCounter, cleaningCounter, cleaningCardCounter);
        }

        [TestMethod]
        [TestCategory("Cleaning Tests")]
        public void SetCleaningParametersTest()
        {
            Assert.IsTrue(_zebraPrinter.SetCleaningParameters(5000, 5));
        }

        #endregion Cleaning Tests

        #region Printer Setup Tests

        [TestMethod]
        [TestCategory("Printer Setup Tests")]
        public void ResetPrinterTest()
        {
            Assert.IsTrue(_zebraPrinter.ResetPrinter());
        }

        [TestMethod]
        [TestCategory("Printer Setup Tests")]
        public void SelfAdjustTest()
        {
            Assert.IsTrue(_zebraPrinter.SelfAdjust());
        }

        [TestMethod]
        [TestCategory("Printer Setup Tests")]
        public void GetChecksumTest()
        {
            var checksum = _zebraPrinter.GetChecksum();
            Assert.IsTrue(checksum >= 0);
            Console.WriteLine("Checksum: {0}", checksum);
        }

        [TestMethod]
        [TestCategory("Printer Setup Tests")]
        public void SetCardFeedingModeTest()
        {
            Assert.IsTrue(_zebraPrinter.SetCardFeedingMode(CardFeedingModeEnum.WithCardFeeder));
        }

        [TestMethod]
        [TestCategory("Printer Setup Tests")]
        public void ClearMediaPathTest()
        {
            Assert.IsTrue(_zebraPrinter.ClearMediaPath());
        }

        [TestMethod]
        [TestCategory("Printer Setup Tests")]
        public void ImmediateParamSaveTest()
        {
            Assert.IsTrue(_zebraPrinter.ImmediateParamSave());
        }

        [TestMethod]
        [TestCategory("Printer Setup Tests")]
        public void SetRelativeXOffsetTest()
        {
            Assert.IsTrue(_zebraPrinter.SetRelativeXOffset(400));
        }

        [TestMethod]
        [TestCategory("Printer Setup Tests")]
        public void SetRelativeYOffsetTest()
        {
            Assert.IsTrue(_zebraPrinter.SetRelativeYOffset(400));
        }

        [TestMethod]
        [TestCategory("Printer Setup Tests")]
        public void SetStartPrintXOffsetTest()
        {
            Assert.IsTrue(_zebraPrinter.SetStartPrintXOffset(400));
        }

        [TestMethod]
        [TestCategory("Printer Setup Tests")]
        public void SetStartPrintYOffsetTest()
        {
            Assert.IsTrue(_zebraPrinter.SetStartPrintYOffset(400));
        }

        [TestMethod]
        [TestCategory("Printer Setup Tests")]
        public void SetStartPrintSideBOffsetTest()
        {
            Assert.IsTrue(_zebraPrinter.SetStartPrintSideBOffset(AxisEnum.X, 500));
        }

        [TestMethod]
        [TestCategory("Printer Setup Tests Tests")]
        public void SetStartPrintSideBXOffsetTest()
        {
            Assert.IsTrue(_zebraPrinter.SetStartPrintSideBXOffset(500));
        }

        [TestMethod]
        [TestCategory("Printer Setup Tests Tests")]
        public void SetStartPrintSideBYOffsetTest()
        {
            Assert.IsTrue(_zebraPrinter.SetStartPrintSideBYOffset(500));
        }

        [TestMethod]
        [TestCategory("Printer Setup Tests Tests")]
        public void GetIPAddressTest()
        {
            var ipAddress = _zebraPrinter.GetIPAddress();
            UnitTestHelpers.AssertIsNotNullOrEmptyString(ipAddress);
        }

        #endregion Printer Setup Tests

        #region Image Buffer Tests

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void SetColorContrastTest()
        {
            Assert.IsTrue(_zebraPrinter.SetColorContrast(ColorBufferEnum.Cyan, 5));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void SetContrastIntensityLevelTest()
        {
            Assert.IsTrue(_zebraPrinter.SetContrastIntensityLevel(ColorBufferEnum.Cyan, 5));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void SetHologramIntensityTest()
        {
            Assert.IsTrue(_zebraPrinter.SetHologramIntensity(5));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void SetMonoContrastTest()
        {
            Assert.IsTrue(_zebraPrinter.SetMonoContrast(5));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void SetMonoIntensityTest()
        {
            Assert.IsTrue(_zebraPrinter.SetMonoIntensity(5));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void ClearSpecifiedBmp()
        {
            Assert.IsTrue(_zebraPrinter.ClearSpecifiedBmp(ColorBufferEnum.Cyan));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void ClearMonoImageBufferTest()
        {
            Assert.IsTrue(_zebraPrinter.ClearMonoImageBuffer(VarnishTypeEnum.KResinImageBuffer));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void ClearMonoImageBuffersTest()
        {
            Assert.IsTrue(_zebraPrinter.ClearMonoImageBuffers(MonoBufferEnum.KBuffer));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void ClearColorImageBuffersTest()
        {
            Assert.IsTrue(_zebraPrinter.ClearColorImageBuffers());
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void ClearColorImageBuffferTest()
        {
            Assert.IsTrue(_zebraPrinter.ClearColorImageBufffer(ColorBufferEnum.Cyan));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void PrintMonoImageBufferTest()
        {
            Assert.IsTrue(_zebraPrinter.PrintMonoImageBuffer());
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void PrintMonoImageBufferExTest()
        {
            Assert.IsTrue(_zebraPrinter.PrintMonoImageBufferEx(PostPrintCommandEnum.PrintEjectCard));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void PrintColorImageBufferTest()
        {
            Assert.IsTrue(_zebraPrinter.PrintColorImageBuffer(ColorBufferEnum.Cyan));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void PrintHologramOverlayTest()
        {
            Assert.IsTrue(_zebraPrinter.PrintHologramOverlay(PostPrintCommandEnum.PrintEjectCard));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void PrintCardPanelTest()
        {
            Assert.IsTrue(_zebraPrinter.PrintCardPanel(ColorBufferEnum.Cyan));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void PrintMonoPanelTest()
        {
            Assert.IsTrue(_zebraPrinter.PrintMonoPanel());
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void WriteBoxTest()
        {
            Assert.IsTrue(_zebraPrinter.WriteBox(0, 0, 500, 250, 5));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void WriteBoxExTest()
        {
            Assert.IsTrue(_zebraPrinter.WriteBoxEx(0, 0, 500, 250, 5, GraphicModeEnum.ClearPrintAreaLoadReverseBitmapImage, true));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void WriteTextTest()
        {
            Assert.IsTrue(_zebraPrinter.WriteText(0, 0, RotationEnum.OriginLowerLeftNoRotation, true, 104, "Text to write into the buffer"));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void WriteTextExTest()
        {
            Assert.IsTrue(_zebraPrinter.WriteTextEx(0, 0, RotationEnum.OriginLowerLeftNoRotation, true, 0, 104, GraphicModeEnum.ClearPrintAreaLoadReverseBitmapImage, "Text to write into the buffer", true));
        }

        [TestMethod]
        [TestCategory("Image Buffer Tests")]
        public void SetEndOfPrintTest()
        {
            Assert.IsTrue(_zebraPrinter.SetEndOfPrint(300));
        }

        #endregion Image Buffer Tests

        #region Position Card Tests

        [TestMethod]
        [TestCategory("Position Card Tests")]
        public void ReversePrintReadyTest()
        {
            Assert.IsTrue(_zebraPrinter.ReversePrintReady());
        }

        [TestMethod]
        [TestCategory("Position Card Tests")]
        public void EjectCardTest()
        {
            Assert.IsTrue(_zebraPrinter.EjectCard());
        }

        [TestMethod]
        [TestCategory("Position Card Tests")]
        public void FlipCardTest()
        {
            Assert.IsTrue(_zebraPrinter.FlipCard());
        }

        [TestMethod]
        [TestCategory("Position Card Tests")]
        public void MoveCardTest()
        {
            Assert.IsTrue(_zebraPrinter.MoveCard(100));
        }

        [TestMethod]
        [TestCategory("Position Card Tests")]
        public void MoveCardBackwardTest()
        {
            Assert.IsTrue(_zebraPrinter.MoveCardBackward(100));
        }

        [TestMethod]
        [TestCategory("Position Card Tests")]
        public void MoveCardForward()
        {
            Assert.IsTrue(_zebraPrinter.MoveCardForward(100));
        }

        [TestMethod]
        [TestCategory("Position Card Tests")]
        public void ResyncTest()
        {
            Assert.IsTrue(_zebraPrinter.Resync());
        }

        [TestMethod]
        [TestCategory("Position Card Tests")]
        public void StartContactSmartCard()
        {
            Assert.IsTrue(_zebraPrinter.StartSmartCard(SmartCardTypeEnum.Contact));
        }

        [TestMethod]
        [TestCategory("Position Card Tests")]
        public void EndSmartCard()
        {
            Assert.IsTrue(_zebraPrinter.EndSmartCard(SmartCardTypeEnum.Contact, CardMovementEnum.EjectCard));
        }

        #endregion Position Card Tests

        #region Test Card Tests

        [TestMethod]
        [TestCategory("Test Card Tests")]
        public void PrintLaminationTestCard()
        {
            Assert.IsTrue(_zebraPrinter.PrintTestCard(TestCardTypeEnum.LaminationTestCard));
        }

        [TestMethod]
        [TestCategory("Test Card Tests")]
        public void PrintMagneticEncoderTestCard()
        {
            Assert.IsTrue(_zebraPrinter.PrintTestCard(TestCardTypeEnum.MagneticEncoderTestCard));
        }

        [TestMethod]
        [TestCategory("Test Card Tests")]
        public void PrintPrinterTestCard()
        {
            Assert.IsTrue(_zebraPrinter.PrintTestCard(TestCardTypeEnum.PrinterTestCard));
        }

        [TestMethod]
        [TestCategory("Test Card Tests")]
        public void PrintStandardTestCard()
        {
            Assert.IsTrue(_zebraPrinter.PrintTestCard(TestCardTypeEnum.StandardTestCard));
        }

        #endregion Test Card Tests

        #region Upgrade Tests

        [TestMethod]
        [TestCategory("Upgrade Tests")]
        public void UpgradeFirmware()
        {
            Assert.IsTrue(_zebraPrinter.UpgradeFirmware("new_firmware.bin"));
        }

        #endregion

        #region Unit Converters Tests

        [TestMethod]
        [TestCategory("Unit Converters Tests")]
        public void InchesToDotsTest()
        {
            var dots = ZXPSeries13.InchesToDots(0.25);
            Assert.AreEqual(75, dots);
        }

        [TestMethod]
        [TestCategory("Unit Converters Tests")]
        public void MillimetersToDotsTest()
        {
            var dots = ZXPSeries13.MillimetersToDots(6.35);
            Assert.AreEqual(75, dots);
        }

        #endregion Unit Converters Tests
    }
}