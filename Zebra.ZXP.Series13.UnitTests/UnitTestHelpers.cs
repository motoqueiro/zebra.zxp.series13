﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Zebra.ZXP.Series13.UnitTests
{
    public static class UnitTestHelpers
    {
        public static void AssertIsNotNullOrEmptyString(string value)
        {
            Assert.IsFalse(string.IsNullOrEmpty(value));
        }

        public static void AssertIsNullOrEmptyString(string value)
        {
            AssertIsNotNullOrEmptyString(value);
        }

        public static void AssertErrorCode(ErrorCode errorCode)
        {
            Assert.AreNotEqual(errorCode.Code, 0);
            AssertIsNotNullOrEmptyString(errorCode.Description);
            AssertIsNotNullOrEmptyString(errorCode.PossibleCause);
        }

        public static void AssertPrinterOptions(PrinterOptions printerOptions)
        {
            AssertIsNotNullOrEmptyString(printerOptions.BaseUnit);
            AssertIsNotNullOrEmptyString(printerOptions.FirmwareVersion);
        }

        public static void AssertOperationalParameters(OperationalParameters operationalParameters)
        {
            Assert.IsTrue(operationalParameters.BlackContrast.HasValue);
            AssertIsNotNullOrEmptyString(operationalParameters.BlackPrintingParameter);
            Assert.IsTrue(operationalParameters.BlackSpeed.HasValue);
            Assert.IsTrue(operationalParameters.CoercivitySetting.HasValue);
            Assert.IsTrue(operationalParameters.CyanContrast.HasValue);
            Assert.IsTrue(operationalParameters.CyanIntensity.HasValue);
            Assert.IsTrue(operationalParameters.EncoderHeadPlacement.HasValue);
            Assert.IsTrue(operationalParameters.HologramContrast.HasValue);
            Assert.IsTrue(operationalParameters.KdyeContrast.HasValue);
            Assert.IsTrue(operationalParameters.KdyeIntensity.HasValue);
            Assert.IsTrue(operationalParameters.MagentaContrast.HasValue);
            Assert.IsTrue(operationalParameters.MagentaIntensity.HasValue);
            Assert.IsTrue(operationalParameters.MagneticEncoder.HasValue);
            Assert.IsTrue(operationalParameters.MagneticEncodingFormat.HasValue);
            Assert.IsTrue(operationalParameters.P1SettingPlusEC.HasValue);
            Assert.IsTrue(operationalParameters.P1SettingSXYCommand.HasValue);
            Assert.IsTrue(operationalParameters.PrintHeadResistance.HasValue);
            Assert.IsTrue(operationalParameters.SmartCardOffset.HasValue);
            Assert.IsTrue(operationalParameters.VarnishContrast.HasValue);
            Assert.IsTrue(operationalParameters.VarnishSpeed.HasValue);
            Assert.IsTrue(operationalParameters.XOffset.HasValue);
            Assert.IsTrue(operationalParameters.YellowContrast.HasValue);
            Assert.IsTrue(operationalParameters.YellowIntensity.HasValue);
            Assert.IsTrue(operationalParameters.YOffset.HasValue);
        }

        public static void AssertSensorStatus(SensorStatus sensorStatus)
        {
            
        }
    }
}