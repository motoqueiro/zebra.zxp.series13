﻿using System.Collections;
using Zebra.ZXP.Series13.Enums;

namespace Zebra.ZXP.Series13
{
    public class SensorStatus
    {
        public DoorStatusEnum MainDoor { get; set; }

        public RibbonSensorStatusEnum RibbonSensor { get; set; }

        public SynchroStatusEnum ATMSensor { get; set; }

        public SynchroStatusEnum PrintSynchroSensor { get; set; }

        public DoorStatusEnum FeederDoor { get; set; }

        public FlipperPositionStatusEnum FlipperPosition { get; set; }

        public CardPresentEnum CardInFlipper { get; set; }

        public SynchroStatusEnum MagSynchroSensor { get; set; }

        public SensorStatus(byte status)
        {
            var statusBits = new BitArray(status);
            MainDoor = (DoorStatusEnum)ConvertBoolToInt(statusBits[0]);
            RibbonSensor = (RibbonSensorStatusEnum)ConvertBoolToInt(statusBits[1]);
            ATMSensor = (SynchroStatusEnum)ConvertBoolToInt(statusBits[2]);
            PrintSynchroSensor = (SynchroStatusEnum)ConvertBoolToInt(statusBits[3]);
            FeederDoor = (DoorStatusEnum)ConvertBoolToInt(statusBits[4]);
            FlipperPosition = (FlipperPositionStatusEnum)ConvertBoolToInt(statusBits[5]);
            CardInFlipper = (CardPresentEnum)ConvertBoolToInt(statusBits[6]);
            MagSynchroSensor = (SynchroStatusEnum)ConvertBoolToInt(statusBits[7]);
        }

        private int ConvertBoolToInt(bool value)
        {
            return value ? 1 : 0;
        }
    }
}