﻿using System;
using Zebra.ZXP.Series13.Enums;

namespace Zebra.ZXP.Series13
{
    public class OperationalParameters
    {
        [OperationalParameterIndex(0)]
        public string BlackPrintingParameter { get; set; }

        [OperationalParameterIndex(1)]
        public int? XOffset { get; set; }

        [OperationalParameterIndex(2)]
        public int? YOffset { get; set; }

        [OperationalParameterIndex(3)]
        public int? BlackContrast { get; set; }

        [OperationalParameterIndex(4)]
        public int? VarnishContrast { get; set; }

        [OperationalParameterIndex(5)]
        public int? HologramContrast { get; set; }

        [OperationalParameterIndex(6)]
        public int? YellowContrast { get; set; }

        [OperationalParameterIndex(7)]
        public int? MagentaContrast { get; set; }

        [OperationalParameterIndex(8)]
        public int? CyanContrast { get; set; }

        [OperationalParameterIndex(9)]
        public int? KdyeContrast { get; set; }

        [OperationalParameterIndex(10)]
        public int? YellowIntensity { get; set; }

        [OperationalParameterIndex(11)]
        public int? MagentaIntensity { get; set; }

        [OperationalParameterIndex(12)]
        public int? CyanIntensity { get; set; }

        [OperationalParameterIndex(13)]
        public int? KdyeIntensity { get; set; }

        [OperationalParameterIndex(14)]
        public OffsetEnum? P1SettingSXYCommand { get; set; }

        [OperationalParameterIndex(15)]
        public int? PrintHeadResistance { get; set; }

        [OperationalParameterIndex(16)]
        public int? BlackSpeed { get; set; }

        [OperationalParameterIndex(17)]
        public int? VarnishSpeed { get; set; }

        [OperationalParameterIndex(18)]
        public int? P1SettingPlusEC { get; set; }

        [OperationalParameterIndex(19)]
        public int? SmartCardOffset { get; set; }

        [OperationalParameterIndex(20)]
        public ConnectivityEnum? MagneticEncoder { get; set; }

        [OperationalParameterIndex(21)]
        public CoercivityEnum? CoercivitySetting { get; set; }

        [OperationalParameterIndex(22)]
        public MagneticFormatEnum? MagneticEncodingFormat { get; set; }

        [OperationalParameterIndex(23)]
        public EncoderHeadPlacementEnum? EncoderHeadPlacement { get; set; }

        public void SetValue(OperationalParameterEnum operationalParameter, string value)
        {
            int operationalParameterIndex = (int)operationalParameter;
            foreach (var propertyInfo in typeof(OperationalParameters).GetProperties())
            {
                foreach (var attribute in propertyInfo.GetCustomAttributes(false))
                {
                    var operationalParameterIndexAttribute = attribute as OperationalParameterIndex;
                    if (operationalParameterIndexAttribute == null ||
                        (operationalParameterIndexAttribute != null &&
                        operationalParameterIndexAttribute.Index != operationalParameterIndex))
                        continue;
                    var typeCode = Type.GetTypeCode(propertyInfo.PropertyType);
                    if (propertyInfo.PropertyType.IsGenericType &&
                        propertyInfo.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        typeCode = Type.GetTypeCode(Nullable.GetUnderlyingType(propertyInfo.PropertyType));
                    if (string.IsNullOrEmpty(value))
                        propertyInfo.SetValue(this, null);
                    else if (propertyInfo.PropertyType.IsEnum)
                        propertyInfo.SetValue(this, Enum.ToObject(propertyInfo.PropertyType, int.Parse(value)));
                    else if (typeCode == TypeCode.Int32)
                        propertyInfo.SetValue(this, int.Parse(value));
                    else
                        propertyInfo.SetValue(this, value);
                }
            }
        }
    }
}