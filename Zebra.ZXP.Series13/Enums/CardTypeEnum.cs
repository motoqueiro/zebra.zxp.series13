﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum CardTypeEnum
        : int
    {
        ZBR_Synchronous = 1,
        ZBR_ISO_78163 = 2
    }
}