using System.Collections;

namespace Zebra.ZXP.Series13.Enums
{
    public enum RibbonSensorStatusEnum
        : int
    {
        TransparentNoPanel = 0,
        OpaquePanel
    }
}
