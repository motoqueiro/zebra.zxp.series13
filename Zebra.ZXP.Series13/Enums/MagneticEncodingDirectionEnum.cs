﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum MagneticEncodingDirectionEnum
        : int
    {
        Forward = 0,
        Reverse = 1
    }
}