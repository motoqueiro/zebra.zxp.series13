﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum BarcodeTypeEnum
        : int
    {
        Code39 = 0,
        Interleave = 1,
        Industrial = 2,
        EAN8 = 3,
        EAN13 = 4,
        UPCA = 5,
        MONARCH = 6,
        Code128CWithoutCheckDigits = 7,
        Code128BWithoutCheckDigits = 8,
        Code128CWithCheckDigits = 107,
        Code128BWithCheckDigits = 108
    }
}