﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum OffsetEnum
    {
        OriginOffset = 0,
        NoOriginOffset = 1
    }
}