using System.Collections;

namespace Zebra.ZXP.Series13.Enums
{
    public enum FlipperPositionStatusEnum
        : int
    {
        CardFeedPosition = 0,
        CardPrintPosition = 1
    }
}
