using System.Collections;

namespace Zebra.ZXP.Series13.Enums
{
    public enum CardPresentEnum
        : int
    {
        NoCard = 0,
        Card = 1
    }
}
