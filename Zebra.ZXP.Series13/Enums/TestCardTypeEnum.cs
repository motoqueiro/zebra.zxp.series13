﻿namespace Zebra.ZXP.Series13
{
    public enum TestCardTypeEnum
    {
        StandardTestCard = 0,
        PrinterTestCard = 1,
        MagneticEncoderTestCard = 2,
        LaminationTestCard = 3
    }
}