﻿namespace Zebra.ZXP.Series13.Enums
{
    /// <summary>
    /// Available graphic modes:
    /// <list type="bullet">
    /// <item>
    /// <term>0</term>
    /// <description>Clear print area and load reverse bitmap image</description>
    /// </item>
    /// <item>
    /// <term>1</term>
    /// <description>Clear print area and load bit map image</description>
    /// </item>
    /// <item>
    /// <term>2</term>
    /// <description>Merge bit map image with print area</description>
    /// </item>
    /// <item>
    /// </list>
    /// </summary>
    public enum GraphicModeEnum
        : int
    {
        ClearPrintAreaLoadReverseBitmapImage = 0,
        ClearPrintAreaLoadBitmapImage = 1,
        MergeBitMapImageWithPrintArea = 2
    }
}