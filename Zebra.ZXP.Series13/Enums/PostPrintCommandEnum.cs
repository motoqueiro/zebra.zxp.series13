﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum PostPrintCommandEnum
        : int
    {
        PrintEjectCard = 0,
        PrintUsingInvertedImageBufferEjectCard = 1,
        PrintReturnCardPrintReady = 10,
        PrintUsingInvertedImageBufferReturnCardPrintReady = 11,
        PrintReturnCardPrintReadySynchronizesAppropriate = 20,
        PrintLeaveCardInPlace = 30,
        PrintUsingInvertedImageBufferLeaveCardInPlace = 31
}
}