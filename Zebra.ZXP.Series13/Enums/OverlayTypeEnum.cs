﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum OverlayTypeEnum
        : int
    {
        FULL_OVERLAY = 0,
        NO_OVERLAY = 1,
        SELECTED_AREA = 2,
        SELECTED_BLANK = 3,
        BITMAP_BASED = 4,
        SMART_ISO = 5,
        SMART_AFNOR = 6,
        MAG_STRIPE = 7
    }
}