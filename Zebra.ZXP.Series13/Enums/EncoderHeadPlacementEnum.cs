﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum EncoderHeadPlacementEnum
    {
        BelowCardPath,
        AboveCardPath
    }
}