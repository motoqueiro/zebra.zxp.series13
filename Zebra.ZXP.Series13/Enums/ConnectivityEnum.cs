﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum ConnectivityEnum
    {
        NotConnected = 0,
        Connected = 1
    }
}