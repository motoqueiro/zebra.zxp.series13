﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum OverlaySideEnum
        : int
    {
        Front = 0,
        Back = 1
    }
}