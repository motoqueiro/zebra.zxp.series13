﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum MonoBufferEnum
        : int
    {
        KBuffer = 0,
        VarnishBuffer = 1
    }
}