﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum CardFeedingModeEnum
        : int
    {
        WithCardFeeder = 0,
        WithoutCardFeeder = 1
    }
}