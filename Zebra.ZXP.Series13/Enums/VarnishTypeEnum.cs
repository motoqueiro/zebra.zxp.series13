﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum VarnishTypeEnum
        : int
    {
        KResinImageBuffer = 0,
        VarnishOverlayImageBuffer = 1
    }
}