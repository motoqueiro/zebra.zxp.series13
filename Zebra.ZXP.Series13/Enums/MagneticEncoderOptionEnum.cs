﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum MagneticEncoderOptionEnum
    {
        NoMagneticEncoder,
        MagneticEncoder
    }
}