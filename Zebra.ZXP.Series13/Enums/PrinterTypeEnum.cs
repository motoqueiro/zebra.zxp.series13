﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum PrinterTypeEnum
        : int
    {
        SingleSided = 31,
        DualSided = 32
    }
}