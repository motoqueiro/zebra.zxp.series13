using System.Collections;

namespace Zebra.ZXP.Series13.Enums
{
    public enum DoorStatusEnum
        : int
    {
        Closed = 0,
        Open = 1
    }
}
