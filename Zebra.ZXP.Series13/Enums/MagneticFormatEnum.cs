﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum MagneticFormatEnum
    {
        JIS2,
        ISO
    }
}