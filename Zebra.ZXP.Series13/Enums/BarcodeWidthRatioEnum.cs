﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum BarcodeWidthRatioEnum
        : int
    {
        NarrowBar1DotWideBar2Dots = 0,
        NarrowBar1DotWideBar3Dots = 1,
        NarrowBar2DotWideBar5Dots = 2
    }
}