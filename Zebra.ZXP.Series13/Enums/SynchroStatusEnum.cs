using System.Collections;

namespace Zebra.ZXP.Series13.Enums
{
    public enum SynchroStatusEnum
        : int
    {
        Clear = 0,
        Blocked = 1
    }
}
