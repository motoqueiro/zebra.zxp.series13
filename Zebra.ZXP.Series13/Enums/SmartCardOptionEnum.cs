﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum SmartCardOptionEnum
    {
        NoSmartCardEncoder,
        ContactContactlessSmartCardEncoders,
        ContactSmartCardEncoder
    }
}