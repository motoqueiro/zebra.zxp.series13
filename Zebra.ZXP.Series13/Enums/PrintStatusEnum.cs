﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum PrintStatusEnum
    {
        StartedSmartCardEncoding,
        TerminatedSmartCardEncoding,
        StartedMagneticBandEncoding,
        TerminatedMagneticBandEncoding,
        StartedPrinting,
        TerminatedPrinting
    }
}