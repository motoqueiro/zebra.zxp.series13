﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum OperatingModeEnum
        : short
    {
        ZBR_ISO_MODE = 0,
        ZBR_EMV_MODE = 1
    }
}