﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum SecurityOptionEnum
    {
        NoEnclosureLocks,
        EnclosureLocks
    }
}