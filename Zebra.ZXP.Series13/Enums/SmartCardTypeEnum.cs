﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum SmartCardTypeEnum
        : int
    {
        Contact = 1,
        Contactless = 2,
        UHF = 3
    }
}