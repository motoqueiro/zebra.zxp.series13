﻿namespace Zebra.ZXP.Series13
{
    public enum MessageSupressionStateEnum
        : int
    {
        Unknown = -1,
        SuppressionDisabled = 0,
        SuppressionEnabled = 1
    }
}