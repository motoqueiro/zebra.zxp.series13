﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum ColorBufferEnum
        : int
    {
        Yellow = 0,
        Magenta = 1,
        Cyan = 2,
        DyeSublimationBlack = 3
    }
}