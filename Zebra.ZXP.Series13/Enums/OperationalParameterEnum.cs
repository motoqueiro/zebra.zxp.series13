﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum OperationalParameterEnum
        : int
    {
        BlackPrintingParameter = 0,
        XOffset = 1,
        YOffset = 2,
        BlackContrast = 3,
        VarnishContrast = 4,
        HologramContrast = 5,
        YellowContrast = 6,
        MagentaContrast = 7,
        CyanContrast = 8,
        KdyeContrast = 9,
        YellowIntensity = 10,
        MagentaIntensity = 11,
        CyanIntensity = 12,
        KdyeIntensity = 13,
        P1SettingSXYVommand = 14,
        PrintHeadResistance = 15,
        BlackSpeed = 16,
        VarnishSpeed = 17,
        P1SettingEC = 18,
        SmartCardOffset = 19,
        MagneticEncoder = 20,
        CoercivitySetting = 21,
        MagneticEncodingFormat = 22,
        EncoderHeadPlacement = 23
    }
}