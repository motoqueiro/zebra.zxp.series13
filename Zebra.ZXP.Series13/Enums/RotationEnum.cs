﻿namespace Zebra.ZXP.Series13.Enums
{
    /// <summary>
    /// Available rotation values:
    /// <list type="bullet">
    /// <item>
    /// <term>0</term>
    /// <description>Origin lower left no rotation</description>
    /// </item>
    /// <item>
    /// <term>1</term>
    /// <description>Origin lower left 90 degrees</description>
    /// </item>
    /// <item>
    /// <term>2</term>
    /// <description>Origin lower left 180 degrees</description>
    /// </item>
    /// <item>
    /// <term>3</term>
    /// <description>origin lower left 270 degrees</description>
    /// </item>
    /// <item>
    /// <term>4</term>
    /// <description>Origin center no rotation</description>
    /// </item>
    /// <item>
    /// <term>5</term>
    /// <description>Origin center 90 degrees</description>
    /// </item>
    /// <item>
    /// <term>6</term>
    /// <description>Origin center 180 degrees</description>
    /// </item>
    /// <item>
    /// <term>7</term>
    /// <description>origin center 270 degrees</description>
    /// </item>
    /// </list>
    /// </summary>
    public enum RotationEnum
        : int
    {
        OriginLowerLeftNoRotation = 0,
        OriginLowerLeft90Degrees = 1,
        OriginLowerLeft180Degrees = 2,
        OriginLowerLeft270Degrees = 3,
        OriginCenterNoRotation = 4,
        OriginCenter90Degrees = 5,
        OriginCenter180Degrees = 6,
        OriginCenter270Degrees = 7
    }
}