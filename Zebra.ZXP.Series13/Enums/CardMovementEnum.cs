﻿namespace Zebra.ZXP.Series13.Enums
{
    public enum CardMovementEnum
        : int
    {
        MoveCardToPrintReadyPosition = 0,
        EjectCard = 1
    }
}