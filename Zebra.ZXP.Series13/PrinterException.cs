﻿using System;

namespace Zebra.ZXP.Series13
{
    public class PrinterException
        : Exception
    {
        public ErrorCode ErrorCode { get; private set; }

        public PrinterException(int code)
        {
            ErrorCode = ErrorCodeHelper.GetErrorCode(code);
        }

        public override string Message
        {
            get
            {
                return string.Format("Code: {0} | Error: {1} | Possible Cause: {2}", ErrorCode.Code, ErrorCode.Description, ErrorCode.PossibleCause);
            }
        }
    }
}