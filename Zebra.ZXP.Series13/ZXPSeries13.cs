﻿using System;
using System.Linq;
using System.Text;
using Zebra.ZXP.Series13.Enums;

namespace Zebra.ZXP.Series13
{
    public class ZXPSeries13
        : IDisposable
    {
        public string PrinterName { get; private set; }

        /// <summary>
        /// Device context
        /// </summary>
        private IntPtr _handle;

        /// <summary>
        /// Holds the printer type
        /// </summary>
        private PrinterTypeEnum _printerType;

        /// <summary>
        /// Holds the pointer to the current error code
        /// </summary>
        private int _errorCode;

        /// <summary>
        /// Holds the current result
        /// </summary>
        private int _result;

        /// <summary>
        ///
        /// </summary>
        /// <param name="printerName">Printer driver name</param>
        public ZXPSeries13(string printerName)
        {
            if (string.IsNullOrEmpty(printerName))
                throw new ArgumentNullException("printerName");
            PrinterName = printerName;
            _handle = IntPtr.Zero;
            GetHandle();
        }

        private void HandleResult()
        {
            if (_result == 0)
                throw new PrinterException(_errorCode);
        }

        private bool HandleBoolResult()
        {
            HandleResult();
            return _result == 1;
        }

        public IntPtr Handle
        {
            get
            {
                return _handle;
            }
        }

        /// <summary>
        /// Returns the printer type, dual or single sided
        /// </summary>
        public PrinterTypeEnum PrinterType
        {
            get
            {
                return _printerType;
            }
        }

        #region SDK Specific Functions

        /// <summary>
        ///
        /// </summary>
        /// <param name="major"></param>
        /// <param name="minor"></param>
        /// <param name="engLevel"></param>
        public void GetSDKVer(out int major, out int minor, out int engeneeringLevel)
        {
            ZBRPrinter.ZBRPRNGetSDKVer(out major, out minor, out engeneeringLevel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="major"></param>
        /// <param name="minor"></param>
        /// <param name="engLevel"></param>
        public void GetSDKVsn(out int major, out int minor, out int engeneeringLevel)
        {
            ZBRPrinter.ZBRPRNGetSDKVsn(out major, out minor, out engeneeringLevel);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public string GetSDKProductVer()
        {
            byte[] productVersion;
            int bufferSize;
            _result = ZBRPrinter.ZBRPRNGetSDKProductVer(out productVersion, out bufferSize);
            HandleResult();
            if (_result == 1)
                return Encoding.ASCII.GetString(productVersion, 0, bufferSize - 1);
            return null;
        }

        #endregion SDK Specific Functions

        #region Printer Driver Handle Functions

        /// <summary>
        /// Gets a handle for a printer driver
        /// </summary>
        public void GetHandle()
        {
            int printerType;
            _result = ZBRPrinter.ZBRGetHandle(out _handle, Encoding.ASCII.GetBytes(PrinterName), out printerType, out _errorCode);
            HandleResult();
            _printerType = (PrinterTypeEnum)Enum.ToObject(typeof(PrinterTypeEnum), printerType);
        }

        /// <summary>
        /// Closes a handle for a printer driver
        /// </summary>
        public void CloseHandle()
        {
            _result = ZBRPrinter.ZBRCloseHandle(_handle, out _errorCode);
            HandleResult();
        }

        #endregion Printer Driver Handle Functions

        #region Printer Driver Functions

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public MessageSupressionStateEnum GetMessageSupressionState()
        {
            int state;
            _result = ZBRPrinter.ZBRPRNGetMsgSuppressionState(_handle, (int)_printerType, out state, out _errorCode);
            HandleResult();
            return (MessageSupressionStateEnum)Enum.ToObject(typeof(MessageSupressionStateEnum), state);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public bool SupressStatusMessages(MessageSupressionStateEnum state)
        {
            _result = ZBRPrinter.ZBRPRNSuppressStatusMsgs(_handle, (int)_printerType, (int)state, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="overlaySide"></param>
        /// <param name="overlayType"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool SetOverlayType(OverlaySideEnum overlaySide, OverlayTypeEnum overlayType, string filename)
        {
            _result = ZBRPrinter.ZBRPRNSetOverlayMode(_handle, (int)_printerType, (int)overlaySide, overlayType, filename, out _errorCode);
            return HandleBoolResult();
        }

        #endregion Printer Driver Functions

        #region Printer Command Functions

        /// <summary>
        /// Prints an *.prn file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool PrintPrnFile(string filename)
        {
            _result = ZBRPrinter.ZBRPRNPrintPrnFile(_handle, (int)_printerType, Encoding.ASCII.GetBytes(filename), out _errorCode);
            return HandleBoolResult();
        }

        #endregion Printer Command Functions

        #region Status Functions

        /// <summary>
        /// Queries the printer for its current status
        /// </summary>
        public ErrorCode CheckForErrors()
        {
            _result = ZBRPrinter.ZBRPRNCheckForErrors(_handle, (int)_printerType);
            if (_result == 0)
                return null;
            return ErrorCodeHelper.GetErrorCode(_result);
        }

        /// <summary>
        /// Clears the error status line
        /// </summary>
        public bool ClearErrorStatusLine()
        {
            _result = ZBRPrinter.ZBRPRNClrErrStatusLn(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        /// Gets the total number of cards printed
        /// </summary>
        /// <returns>Total card count</returns>
        public int GetPrintCount()
        {
            int printCount;
            _result = ZBRPrinter.ZBRPRNGetPrintCount(_handle, (int)_printerType, out printCount, out _errorCode);
            HandleResult();
            return printCount;
        }

        /// <summary>
        /// Queries the printer driver for current job status
        /// </summary>
        /// <returns></returns>
        public ErrorCode GetPrintStatus()
        {
            _result = ZBRPrinter.ZBRPRNGetPrintStatus(_handle, (int)_printerType);
            if (_result == 0)
                return null;
            return ErrorCodeHelper.GetErrorCode(_result);
        }

        /// <summary>
        /// Returns the number of ribbon panels remaining
        /// </summary>
        /// <returns></returns>
        public int GetPanelsRemaining()
        {
            int panels;
            _result = ZBRPrinter.ZBRPRNGetPanelsRemaining(_handle, (int)_printerType, out panels, out _errorCode);
            HandleResult();
            return panels;
        }

        /// <summary>
        /// Returns the number of ribbon panels that have been printed
        /// </summary>
        /// <returns>Printed panels</returns>
        public int GetPanelsPrinted()
        {
            int panels;
            _result = ZBRPrinter.ZBRPRNGetPanelsPrinted(_handle, (int)_printerType, out panels, out _errorCode);
            HandleResult();
            return panels;
        }

        /// <summary>
        /// Retrieves the printer serial number
        /// </summary>
        /// <returns>Serial number</returns>
        public string GetPrinterSerialNumber()
        {
            byte[] serialNumber = null;
            int responseSize;
            _result = ZBRPrinter.ZBRPRNGetPrinterSerialNumber(_handle, (int)_printerType, serialNumber, out responseSize, out _errorCode);
            HandleResult();
            return Encoding.ASCII.GetString(serialNumber, 0, responseSize - 1);
        }

        /// <summary>
        /// Retrieves the printer serial number
        /// </summary>
        /// <returns>Serial number</returns>
        public string GetPrinterSerialNumb()
        {
            byte[] serialNumber = null;
            int responseSize;
            _result = ZBRPrinter.ZBRPRNGetPrinterSerialNumb(_handle, (int)_printerType, serialNumber, out responseSize, out _errorCode);
            HandleResult();
            return Encoding.ASCII.GetString(serialNumber, 0, responseSize - 1);
        }

        /// <summary>
        /// Retrieves the printer options
        /// </summary>
        /// <returns></returns>
        public PrinterOptions GetPrinterOptions()
        {
            byte[] options = null;
            int responseSize;
            _result = ZBRPrinter.ZBRPRNGetPrinterOptions(_handle, (int)_printerType, options, out responseSize, out _errorCode);
            HandleResult();
            return new PrinterOptions(Encoding.ASCII.GetString(options, 0, responseSize - 1));
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public string GetPrintHeadSerialNumber()
        {
            byte[] serialNumber = null;
            int responseSize;
            _result = ZBRPrinter.ZBRPRNGetPrintHeadSerialNumber(_handle, (int)_printerType, serialNumber, out responseSize, out _errorCode);
            HandleResult();
            if (_result == 0)
                return null;
            return Encoding.ASCII.GetString(serialNumber, 0, responseSize - 1);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public string GetPrintHeadSerialNumb()
        {
            byte[] serialNumber = null;
            int responseSize;
            _result = ZBRPrinter.ZBRPRNGetPrintHeadSerialNumb(_handle, (int)_printerType, serialNumber, out responseSize, out _errorCode);
            HandleResult();
            if (_result == 0)
                return null;
            return Encoding.ASCII.GetString(serialNumber, 0, responseSize - 1);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="operationalParameter"></param>
        /// <returns></returns>
        public string GetOperationalParameter(OperationalParameterEnum operationalParameter)
        {
            byte[] parameterRawValue = null;
            int responseSize;
            _result = ZBRPrinter.ZBRPRNGetOpParam(_handle, (int)_printerType, (int)operationalParameter, parameterRawValue, out responseSize, out _errorCode);
            HandleResult();
            return Encoding.ASCII.GetString(parameterRawValue, 0, responseSize - 1);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public OperationalParameters GetOperationalParameters()
        {
            var result = new OperationalParameters();
            foreach (OperationalParameterEnum operationalParameter in Enum.GetValues(typeof(OperationalParameterEnum)).Cast<OperationalParameterEnum>())
            {
                var parameterValue = GetOperationalParameter(operationalParameter);
                result.SetValue(operationalParameter, parameterValue);
            }
            return result;
        }

        /// <summary>
        /// Returns the printer's current status code
        /// </summary>
        /// <returns></returns>
        public ErrorCode GetPrinterStatus()
        {
            _result = ZBRPrinter.ZBRPRNGetPrinterStatus(out _errorCode);
            if (_result == 0)
                return null;
            return ErrorCodeHelper.GetErrorCode(_errorCode);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public SensorStatus GetSensorStatus()
        {
            byte status;
            _result = ZBRPrinter.ZBRPRNGetSensorStatus(_handle, (int)_printerType, out status, out _errorCode);
            HandleResult();
            return new SensorStatus(status);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool IsPrinterReady()
        {
            _result = ZBRPrinter.ZBRPRNIsPrinterReady(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        #endregion Status Functions

        #region Cleaning Functions

        /// <summary>
        ///
        /// </summary>
        /// <param name="printingCounter"></param>
        /// <param name="cleaningCounter"></param>
        /// <param name="cleaningCardCounter"></param>
        public bool CheckDueForCleaning(out int printingCounter, out int cleaningCounter, out int cleaningCardCounter)
        {
            _result = ZBRPrinter.ZBRPRNChkDueForCleaning(_handle, (int)_printerType, out printingCounter, out cleaningCounter, out cleaningCardCounter, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        /// Starts a cleaning sequence
        /// </summary>
        /// <returns></returns>
        public bool StartCleaningSequence()
        {
            _result = ZBRPrinter.ZBRPRNStartCleaningSeq(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="printingCounter"></param>
        /// <param name="cleaningCounter"></param>
        /// <param name="cleaningCardCounter"></param>
        /// <returns></returns>
        public bool GetCleaningParameters(out int printingCounter, out int cleaningCounter, out int cleaningCardCounter)
        {
            _result = ZBRPrinter.ZBRPRNGetCleaningParam(_handle, (int)_printerType, out printingCounter, out cleaningCounter, out cleaningCardCounter, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="ribbonPanelCounter"></param>
        /// <param name="cleanCardPasses"></param>
        /// <returns></returns>
        public bool SetCleaningParameters(int ribbonPanelCounter, int cleanCardPasses)
        {
            _result = ZBRPrinter.ZBRPRNSetCleaningParam(_handle, (int)_printerType, ribbonPanelCounter, cleanCardPasses, out _errorCode);
            return HandleBoolResult();
        }

        #endregion Cleaning Functions

        #region Printer Setup Functions

        /// <summary>
        /// Resets printer
        /// </summary>
        /// <returns></returns>
        public bool ResetPrinter()
        {
            _result = ZBRPrinter.ZBRPRNResetPrinter(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool SelfAdjust()
        {
            _result = ZBRPrinter.ZBRPRNSelfAdj(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public int GetChecksum()
        {
            int checksum;
            _result = ZBRPrinter.ZBRPRNGetChecksum(_handle, (int)_printerType, out checksum, out _errorCode);
            HandleResult();
            if (_result == 1)
                return checksum;
            return -1;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="cardFeedingMode"></param>
        /// <returns></returns>
        public bool SetCardFeedingMode(CardFeedingModeEnum cardFeedingMode)
        {
            _result = ZBRPrinter.ZBRPRNSetCardFeedingMode(_handle, (int)_printerType, (int)cardFeedingMode, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="resistance"></param>
        /// <returns></returns>
        public bool SetPrintHeadResistance(int resistance)
        {
            _result = ZBRPrinter.ZBRPRNSetPrintHeadResistance(_handle, (int)_printerType, resistance, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool ClearMediaPath()
        {
            _result = ZBRPrinter.ZBRPRNClrMediaPath(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool ImmediateParamSave()
        {
            _result = ZBRPrinter.ZBRPRNImmediateParamSave(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        public bool SetRelativeXOffset(int offset)
        {
            _result = ZBRPrinter.ZBRPRNSetRelativeXOffset(_handle, (int)_printerType, offset, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        public bool SetRelativeYOffset(int offset)
        {
            _result = ZBRPrinter.ZBRPRNSetRelativeYOffset(_handle, (int)_printerType, offset, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        public bool SetStartPrintXOffset(int offset)
        {
            _result = ZBRPrinter.ZBRPRNSetStartPrintXOffset(_handle, (int)_printerType, offset, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        public bool SetStartPrintYOffset(int offset)
        {
            _result = ZBRPrinter.ZBRPRNSetStartPrintYOffset(_handle, (int)_printerType, offset, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public bool SetStartPrintSideBOffset(AxisEnum axis, int offset)
        {
            if (PrinterType != PrinterTypeEnum.DualSided)
                throw new NotSupportedException("This function is only supported in dual-sided printers");
            _result = ZBRPrinter.ZBRPRNSetStartPrintSideBOffset(_handle, (int)_printerType, (int)axis, offset, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        public bool SetStartPrintSideBXOffset(int offset)
        {
            if (PrinterType != PrinterTypeEnum.DualSided)
                throw new NotSupportedException("This function is only supported in dual-sided printers");
            _result = ZBRPrinter.ZBRPRNSetStartPrintSideBXOffset(_handle, (int)_printerType, offset, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        public bool SetStartPrintSideBYOffset(int offset)
        {
            if (PrinterType != PrinterTypeEnum.DualSided)
                throw new NotSupportedException("This function is only supported in dual-sided printers");
            _result = ZBRPrinter.ZBRPRNSetStartPrintSideBYOffset(_handle, (int)_printerType, offset, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public string GetIPAddress()
        {
            byte[] ipAddress;
            _result = ZBRPrinter.ZBRPRNGetIPAddress(PrinterName, out ipAddress);
            HandleResult();
            if (_result == 1)
                return Encoding.ASCII.GetString(ipAddress, 0, ipAddress.Length - 1);
            return null;
        }

        #endregion Printer Setup Functions

        #region Image Buffer Functions

        private void CheckContrastRange(int contrast)
        {
            if (contrast < 0 || contrast > 10)
                throw new ArgumentOutOfRangeException("contrast");
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="colorBuffer"></param>
        /// <param name="contrast"></param>
        /// <returns></returns>
        public bool SetColorContrast(ColorBufferEnum colorBuffer, int contrast)
        {
            CheckContrastRange(contrast);
            _result = ZBRPrinter.ZBRPRNSetColorContrast(_handle, (int)_printerType, (int)colorBuffer, contrast, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="colorBuffer"></param>
        /// <param name="contrast"></param>
        /// <returns></returns>
        public bool SetContrastIntensityLevel(ColorBufferEnum colorBuffer, int contrast)
        {
            CheckContrastRange(contrast);
            _result = ZBRPrinter.ZBRPRNSetColorContrast(_handle, (int)_printerType, (int)colorBuffer, contrast, out _errorCode);
            HandleResult();
            return _result == 1;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="intensity"></param>
        /// <returns></returns>
        public bool SetHologramIntensity(int intensity)
        {
            CheckContrastRange(intensity);
            _result = ZBRPrinter.ZBRPRNSetHologramIntensity(_handle, (int)_printerType, intensity, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="contrast"></param>
        /// <returns></returns>
        public bool SetMonoContrast(int contrast)
        {
            CheckContrastRange(contrast);
            _result = ZBRPrinter.ZBRPRNSetMonoContrast(_handle, (int)_printerType, contrast, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="intensity"></param>
        /// <returns></returns>
        public bool SetMonoIntensity(int intensity)
        {
            CheckContrastRange(intensity);
            _result = ZBRPrinter.ZBRPRNSetMonoIntensity(_handle, (int)_printerType, intensity, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="colorBuffer"></param>
        /// <returns></returns>
        public bool ClearSpecifiedBmp(ColorBufferEnum colorBuffer)
        {
            _result = ZBRPrinter.ZBRPRNClrSpecifiedBmp(_handle, (int)_printerType, (int)colorBuffer, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="varnishType"></param>
        /// <returns></returns>
        public bool ClearMonoImageBuffer(VarnishTypeEnum varnishType)
        {
            _result = ZBRPrinter.ZBRPRNClrMonoImgBuf(_handle, (int)_printerType, (int)varnishType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="monoBuffer"></param>
        /// <returns></returns>
        public bool ClearMonoImageBuffers(MonoBufferEnum monoBuffer)
        {
            _result = ZBRPrinter.ZBRPRNClrMonoImgBufs(_handle, (int)_printerType, (int)monoBuffer, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool ClearColorImageBuffers()
        {
            _result = ZBRPrinter.ZBRPRNClrColorImgBufs(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="colorBuffer"></param>
        /// <returns></returns>
        public bool ClearColorImageBufffer(ColorBufferEnum colorBuffer)
        {
            _result = ZBRPrinter.ZBRPRNClrColorImgBuf(_handle, (int)_printerType, (int)colorBuffer, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool PrintMonoImageBuffer()
        {
            _result = ZBRPrinter.ZBRPRNPrintMonoImgBuf(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="postPrintCommand"></param>
        /// <returns></returns>
        public bool PrintMonoImageBufferEx(PostPrintCommandEnum postPrintCommand)
        {
            _result = ZBRPrinter.ZBRPRNPrintMonoImgBufEx(_handle, (int)_printerType, (int)postPrintCommand, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="colorBuffer"></param>
        /// <returns></returns>
        public bool PrintColorImageBuffer(ColorBufferEnum colorBuffer)
        {
            _result = ZBRPrinter.ZBRPRNPrintColorImgBuf(_handle, (int)_printerType, (int)colorBuffer, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool PrintClearVarnish()
        {
            _result = ZBRPrinter.ZBRPRNPrintClearVarnish(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool PrintVarnish()
        {
            _result = ZBRPrinter.ZBRPRNPrintVarnish(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="postPrintCommand"></param>
        /// <returns></returns>
        public bool PrintVarnishEx(PostPrintCommandEnum postPrintCommand)
        {
            _result = ZBRPrinter.ZBRPRNPrintVarnishEx(_handle, (int)_printerType, (int)postPrintCommand, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="postPrintCommand"></param>
        /// <returns></returns>
        public bool PrintHologramOverlay(PostPrintCommandEnum postPrintCommand)
        {
            _result = ZBRPrinter.ZBRPRNPrintHologramOverlay(_handle, (int)_printerType, (int)postPrintCommand, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="colorBuffer"></param>
        /// <returns></returns>
        public bool PrintCardPanel(ColorBufferEnum colorBuffer)
        {
            _result = ZBRPrinter.ZBRPRNPrintCardPanel(_handle, (int)_printerType, (int)colorBuffer, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool PrintMonoPanel()
        {
            _result = ZBRPrinter.ZBRPRNPrintMonoPanel(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="thickness"></param>
        /// <returns></returns>
        public bool WriteBox(int x, int y, int width, int height, int thickness)
        {
            _result = ZBRPrinter.ZBRPRNWriteBox(_handle, (int)_printerType, x, y, width, height, thickness, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="thickness"></param>
        /// <param name="graphicMode"></param>
        /// <param name="varnishOverlay"></param>
        /// <returns></returns>
        public bool WriteBoxEx(int x, int y, int width, int height, int thickness, GraphicModeEnum graphicMode, bool isVarnish)
        {
            _result = ZBRPrinter.ZBRPRNWriteBoxEx(_handle, (int)_printerType, x, y, width, height, thickness, (int)graphicMode, Convert.ToInt32(isVarnish), out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="rotation"></param>
        /// <param name="isBold"></param>
        /// <param name="height"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public bool WriteText(int x, int y, RotationEnum rotation, bool isBold, int height, string text)
        {
            var textRaw = Encoding.ASCII.GetBytes(text);
            _result = ZBRPrinter.ZBRPRNWriteText(_handle, (int)PrinterType, x, y, (int)rotation, Convert.ToInt32(isBold), height, textRaw, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="rotation"></param>
        /// <param name="isBold"></param>
        /// <param name="widht"></param>
        /// <param name="height"></param>
        /// <param name="graphicMode"></param>
        /// <param name="text"></param>
        /// <param name="isVarnish"></param>
        /// <returns></returns>
        public bool WriteTextEx(int x, int y, RotationEnum rotation, bool isBold, int widht, int height, GraphicModeEnum graphicMode, string text, bool isVarnish)
        {
            var textRaw = Encoding.ASCII.GetBytes(text);
            _result = ZBRPrinter.ZBRPRNWriteTextEx(_handle, (int)PrinterType, x, y, (int)rotation, Convert.ToInt32(isBold), widht, height, (int)graphicMode, textRaw, Convert.ToInt32(isVarnish), out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="xWidth"></param>
        /// <returns></returns>
        public bool SetEndOfPrint(int xWidth)
        {
            _result = ZBRPrinter.ZBRPRNSetEndOfPrint(_handle, (int)_printerType, xWidth, out _errorCode);
            return HandleBoolResult();
        }

        #endregion Image Buffer Functions

        #region Position Card Functions

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool MovePrintReady()
        {
            _result = ZBRPrinter.ZBRPRNMovePrintReady(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool ReversePrintReady()
        {
            _result = ZBRPrinter.ZBRPRNReversePrintReady(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool EjectCard()
        {
            _result = ZBRPrinter.ZBRPRNEjectCard(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool FlipCard()
        {
            _result = ZBRPrinter.ZBRPRNFlipCard(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="steps">Number of steps to move card (Note: 100 steps = 8mm/0.315in)</param>
        /// <returns></returns>
        public bool MoveCard(int steps)
        {
            _result = ZBRPrinter.ZBRPRNMoveCard(_handle, (int)_printerType, steps, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="steps"></param>
        /// <returns></returns>
        public bool MoveCardBackward(int steps)
        {
            _result = ZBRPrinter.ZBRPRNMoveCardBkwd(_handle, (int)_printerType, steps, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="steps"></param>
        /// <returns></returns>
        public bool MoveCardForward(int steps)
        {
            _result = ZBRPrinter.ZBRPRNMoveCardFwd(_handle, (int)_printerType, steps, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool Resync()
        {
            _result = ZBRPrinter.ZBRPRNResync(_handle, (int)_printerType, out _errorCode);
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        public event BeforeSmartCardStartedHandler BeforeSmartCardStartedEvent;

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        public delegate void BeforeSmartCardStartedHandler(object sender, SmartCardTypeEnum smartCardType);

        /// <summary>
        ///
        /// </summary>
        public event AfterSmartCardStartedHandler AfterSmartCardStartedEvent;

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        public delegate void AfterSmartCardStartedHandler(object sender, SmartCardTypeEnum smartCardType);

        /// <summary>
        ///
        /// </summary>
        /// <param name="smartCardType"></param>
        /// <returns></returns>
        public bool StartSmartCard(SmartCardTypeEnum smartCardType)
        {
            /*if (BeforeSmartCardStartedEvent != null)
                BeforeSmartCardStartedEvent(this, smartCardType);*/
            _result = ZBRPrinter.ZBRPRNStartSmartCard(_handle, (int)_printerType, (int)smartCardType, out _errorCode);
            /*if (_result == 1 && AfterSmartCardStartedEvent != null)
                AfterSmartCardStartedEvent(this, smartCardType);*/
            return HandleBoolResult();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="smartCardType"></param>
        /// <param name="cardMovement"></param>
        /// <returns></returns>
        public bool EndSmartCard(SmartCardTypeEnum smartCardType, CardMovementEnum cardMovement)
        {
            _result = ZBRPrinter.ZBRPRNEndSmartCard(_handle, (int)_printerType, (int)smartCardType, (int)cardMovement, out _errorCode);
            return HandleBoolResult();
        }

        #endregion Position Card Functions

        #region Test Card Functions

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool PrintTestCard(TestCardTypeEnum testCardType)
        {
            _result = ZBRPrinter.ZBRPRNPrintTestCard(_handle, (int)_printerType, (int)testCardType, out _errorCode);
            return HandleBoolResult();
        }

        #endregion Test Card Functions

        #region Upgrade Functions

        /// <summary>
        ///
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool UpgradeFirmware(string filename)
        {
            _result = ZBRPrinter.ZBRPRNUpgradeFirmware(_handle, (int)_printerType, filename, out _errorCode);
            return HandleBoolResult();
        }

        #endregion Upgrade Functions

        #region Barcode Card Functions

        /// <summary>
        ///
        /// </summary>
        /// <param name="startX"></param>
        /// <param name="startY"></param>
        /// <param name="rotation"></param>
        /// <param name="barcodeType"></param>
        /// <param name="ratio"></param>
        /// <param name="barcodeMultiplier"></param>
        /// <param name="barcodeHeight"></param>
        /// <param name="textUnder"></param>
        /// <param name="barcodeData"></param>
        /// <returns></returns>
        public bool WriteBarCode(int startX, int startY, RotationEnum rotation, BarcodeTypeEnum barcodeType, BarcodeWidthRatioEnum ratio, int barcodeMultiplier, int barcodeHeight, bool textUnder, string barcodeData)
        {
            if (barcodeMultiplier < 2 || barcodeMultiplier > 9)
                throw new ArgumentOutOfRangeException("barcodeMultiplier");
            if (string.IsNullOrEmpty(barcodeData))
                throw new ArgumentNullException("barcodeData");
            var barcodeDataRaw = Encoding.ASCII.GetBytes(barcodeData);
            _result = ZBRPrinter.ZBRPRNWriteBarcode(_handle, (int)_printerType, startX, startY, (int)rotation, (int)barcodeType, (int)ratio, barcodeMultiplier, barcodeHeight, Convert.ToInt32(textUnder), barcodeDataRaw, out _errorCode);
            return HandleBoolResult();
        }

        #endregion Barcode Card Functions

        #region Unit Converter Functions

        /// <summary>
        ///
        /// </summary>
        /// <param name="inches"></param>
        /// <returns></returns>
        public static int InchesToDots(double inches)
        {
            return Convert.ToInt32(inches * 300);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="inches"></param>
        /// <returns></returns>
        public static int InchesToSteps(double inches)
        {
            return Convert.ToInt32((inches * 100) / 0.315);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="inches"></param>
        /// <returns></returns>
        public static int MillimetersToSteps(double millimeters)
        {
            return Convert.ToInt32((millimeters * 100) / 8);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="millimeters"></param>
        /// <returns></returns>
        public static int MillimetersToDots(double millimeters)
        {
            var inches = millimeters / 25.4;
            return InchesToDots(inches);
        }

        #endregion Unit Converter Functions

        /// <summary>
        ///
        /// </summary>
        public void Dispose()
        {
            if (_handle != null)
                CloseHandle();
        }
    }
}