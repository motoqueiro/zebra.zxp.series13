﻿using Glintths.SAL.Printer;
using NdefLibrary.Ndef;
using SharpSmartCard;
using SharpSmartCard.Readers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zebra.ZXP.Series13
{
    public class ZXPSeries
        : ZBRPrinter, IDisposable
    {
        public PrinterTypeEnum PrinterType { get; set; }

        private int _errorCode;

        private int _result;

        private Encoding _encoding;

        private NFCEncoder _nfcEncoder;

        private NFCEncoder NfcEncoder
        {
            get
            {
                if (_nfcEncoder == null)
                {
                    var smartCardReader = new SDI010("Identive SDI010 Contactless Reader 0");
                    _nfcEncoder = new NFCEncoder(smartCardReader);
                }
                return _nfcEncoder;
            }
        }

        public delegate void PrintProgressStatus(PrintStatus status);

        public event PrintProgressStatus NotifyPrintProgress;

        public ZXPSeries(string driverName)
            : base()
        {
            Initialize(driverName);
        }

        ~ZXPSeries()
        {
            if (_nfcEncoder != null)
                _nfcEncoder.Dispose();
        }

        private void Initialize(string driverName)
        {
            _encoding = new ASCIIEncoding();
            if (!string.IsNullOrEmpty(driverName))
                Open(driverName);
        }

        private void HandleResult()
        {
            if (_result == 0)
                throw new PrinterException(_errorCode);
        }

        public bool SupportsSmartCardEncoding()
        {
            var printerOptions = GetPrinterOptions();
            return printerOptions.SmartCard != SmartCardOptionEnum.NoSmartCardEncoder;
        }

        public void Print(
            string domainName = null,
            string serverName = null,
            string username = null,
            string password = null,
            string printerIp = null,
            string printerName = null,
            ushort? printerPort = null,
            string documentName = null,
            IEnumerable<byte> printData = null)
        {
            Print(
                domainName,
                serverName,
                username,
                password,
                printerIp,
                printerName,
                printerPort,
                documentName,
                printData,
                null,
                null);
        }

        public void EncodeSmartCard(IEnumerable<byte> nfcData)
        {
            Print(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                nfcData,
                null);
        }

        public void EncodeMagneticBand(IEnumerable<byte> magneticData)
        {
            Print(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                magneticData);
        }

        public void Print(
            string domainName = null,
            string serverName = null,
            string username = null,
            string password = null,
            string printerIp = null,
            string printerName = null,
            ushort? printerPort = null,
            string documentName = null,
            IEnumerable<byte> printData = null,
            IEnumerable<byte> nfcData = null,
            IEnumerable<byte> magneticData = null)
        {
            if (nfcData == null && magneticData == null && printData == null)
                return;
            try
            {
                IsPrinterReady();
                if (nfcData != null)
                {
                    if (!SupportsSmartCardEncoding())
                        throw new Exception("The selected printer does not support smartcard encoding!");
                    NotifyPrintProgress(PrintStatus.StartedSmartCardEncoding);
                    StartSmartCard(CardTypeEnum.ZBRISO78163);
                    NfcEncoder.PublishMessage(nfcData.ToArray());
                    NotifyPrintProgress(PrintStatus.TerminatedSmartCardEncoding);
                    //Mandar codificar cenas
                    if (printData == null)
                    {
                        EndSmartCard(CardTypeEnum.ZBRISO78163, CardMovementEnum.EjectCard);
                        return;
                    }
                    else
                        EndSmartCard(CardTypeEnum.ZBRISO78163, CardMovementEnum.MoveCardToPrintReadyPosition);
                }
                if (magneticData != null)
                {
                    NotifyPrintProgress(PrintStatus.StartedMagneticBandEncoding);
                    NotifyPrintProgress(PrintStatus.TerminatedMagneticBandEncoding);
                }
                if (printData != null)
                {
                    NotifyPrintProgress(PrintStatus.StartedPrinting);
                    RawPrinterHelper.Print(
                        domainName,
                        serverName,
                        username,
                        password,
                        printerIp,
                        printerName,
                        printerPort,
                        documentName,
                        printData.ToArray());
                    NotifyPrintProgress(PrintStatus.TerminatedPrinting);
                    EjectCard();
                }
            }
            catch (Exception ex)
            {
                EjectCard();
                throw;
            }
        }

        public NdefMessage ReadNFCData()
        {
            NdefMessage nfcData = null;
            try
            {
                IsPrinterReady();
                if (!SupportsSmartCardEncoding())
                    throw new Exception("The selected printer does not support smartcard reading!");
                StartSmartCard(CardTypeEnum.ZBRISO78163);
                nfcData = NfcEncoder.ReadMessage();
                EndSmartCard(CardTypeEnum.ZBRISO78163, CardMovementEnum.EjectCard);
            }
            catch (Exception ex)
            {
                EjectCard();
            }
            return nfcData;
        }

        public SensorStatus GetSensorStatus()
        {
            byte status;
            _result = base.GetSensorStatus(out status, out _errorCode);
            HandleResult();
            return new SensorStatus(status);
        }

        public void Open(string driverName)
        {
            _result = base.Open(_encoding.GetBytes(driverName), out _errorCode);
            HandleResult();
        }

        public string GetPrinterSerialNumber()
        {
            var serialNumber = new byte[50];
            int responseSize,
                //_result = base.GetPrinterSerialNumber(serialNumber, out responseSize, out _errorCode);
            _result = base.GetPrinterSerialNumb(serialNumber, out responseSize, out _errorCode);
            HandleResult();
            return _encoding.GetString(serialNumber, 0, responseSize - 1);
        }

        public OperationalParameters GetOperationalParameters()
        {
            var result = new OperationalParameters();
            var result2 = new Dictionary<int, string>();
            var parameterValueRaw = new byte[255];
            for (int i = 0; i <= 23; i++)
            {
                int responseSize;
                _result = base.GetOperationalParameter(i, parameterValueRaw, out responseSize, out _errorCode);
                HandleResult();
                result.SetValue(i, _encoding.GetString(parameterValueRaw, 0, responseSize - 1).Trim());
            }
            return result;
        }

        public PrinterOptions GetPrinterOptions()
        {
            var options = new byte[50];
            int responseSize;
            _result = base.GetPrinterOptions(options, out responseSize, out _errorCode);
            HandleResult();
            return new PrinterOptions(_encoding.GetString(options, 0, responseSize - 1));
        }

        public bool IsPrinterReady()
        {
            _result = base.IsPrinterReady(out _errorCode);
            HandleResult();
            return _errorCode == 0;
        }

        public bool IsPrinterInErrorMode()
        {
            _result = base.GetPrinterStatus(out _errorCode);
            HandleResult();
            return _errorCode == 0;
        }

        public void EjectCard()
        {
            _result = base.EjectCard(out _errorCode);
            HandleResult();
        }

        public void StartSmartCard(CardTypeEnum cardType)
        {
            _result = base.StartSmartCard((int)cardType, out _errorCode);
            HandleResult();
        }

        public void EndSmartCard(CardTypeEnum cardType, CardMovementEnum cardMovement)
        {
            _result = base.EndSmartCard((int)cardType, (int)cardMovement, out _errorCode);
            HandleResult();
        }

        public void MoveCardToPrintReady()
        {
            _result = base.MoveCardToPrintReady(out _errorCode);
            HandleResult();
        }

        private int ConvertMilimitersToSteps(int milimiters)
        {
            return (milimiters * 100) / 8;
        }

        private int ConvertInchesToSteps(double inches)
        {
            return (int)Math.Round((inches * 100) / 0.315, 0);
        }

        public void MoveCardInMilimeters(int milimiters)
        {
            MoveCard(ConvertMilimitersToSteps(milimiters));
        }

        public void MoveCardInInches(double inches)
        {
            MoveCard(ConvertInchesToSteps(inches));
        }

        public void MoveCard(int steps)
        {
            _result = base.MoveCard(steps, out _errorCode);
            HandleResult();
        }

        public void MoveCardForwardInMilimiters(int milimiters)
        {
            MoveCardForward(ConvertMilimitersToSteps(milimiters));
        }

        public void MoveCardForwardInInches(double inches)
        {
            MoveCardForward(ConvertInchesToSteps(inches));
        }

        public void MoveCardForward(int steps)
        {
            _result = base.MoveCardForward(steps, out _errorCode);
            HandleResult();
        }

        public void MoveCardBackwardInMilimiters(int milimiters)
        {
            MoveCardBackward(ConvertMilimitersToSteps(milimiters));
        }

        public void MoveCardBackwardInInches(double inches)
        {
            MoveCardBackward(ConvertInchesToSteps(inches));
        }

        public void MoveCardBackward(int steps)
        {
            _result = base.MoveCardBackward(steps, out _errorCode);
            HandleResult();
        }

        public void ReverseCardToPrintReady()
        {
            _result = base.ReverseCardToPrintReady(out _errorCode);
            HandleResult();
        }

        public void Close()
        {
            _result = base.Close(out _errorCode);
            HandleResult();
        }

        public void FlipCard()
        {
            _result = base.FlipCard(out _errorCode);
            HandleResult();
        }

        public void ResyncCard()
        {
            _result = base.Resync(out _errorCode);
            HandleResult();
        }

        public void Dispose()
        {
            Close();
        }
    }
}