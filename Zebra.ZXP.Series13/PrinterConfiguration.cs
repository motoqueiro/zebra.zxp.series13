﻿using Zebra.ZXP.Series13.Enums;

namespace Zebra.ZXP.Series13
{
    public class PrinterOptions
    {
        public string BaseUnit { get; set; }

        public PrinterTypeEnum Type { get; set; }

        public SmartCardOptionEnum SmartCard { get; set; }

        public MagneticEncoderOptionEnum MagneticEncoder { get; set; }

        public SecurityOptionEnum Security { get; set; }

        public InterfaceOptionEnum Interface { get; set; }

        public string FirmwareVersion { get; set; }

        public PrinterOptions(string serialNumber)
        {
            BaseUnit = serialNumber.Substring(0, 4);
            Type = ResolvePrinterTypeOption(serialNumber);
            SmartCard = ResolveSmartCardOption(serialNumber);
            MagneticEncoder = ResolveMagneticEncoderOption(serialNumber);
            Security = ResolveSecurityOption(serialNumber);
            Interface = ResolveInterfaceOption(serialNumber);
            FirmwareVersion = serialNumber.Substring(10);
        }

        private InterfaceOptionEnum ResolveInterfaceOption(string serialNumber)
        {
            if (serialNumber[8] == 'C')
                return InterfaceOptionEnum.USBAndEthernet;
            return InterfaceOptionEnum.USBOnly;
        }

        private SecurityOptionEnum ResolveSecurityOption(string serialNumber)
        {
            if (serialNumber[7] == 'A')
                return SecurityOptionEnum.EnclosureLocks;
            return SecurityOptionEnum.NoEnclosureLocks;
        }

        private MagneticEncoderOptionEnum ResolveMagneticEncoderOption(string serialNumber)
        {
            if (serialNumber[6] == 'M')
                return MagneticEncoderOptionEnum.MagneticEncoder;
            return MagneticEncoderOptionEnum.NoMagneticEncoder;
        }

        private SmartCardOptionEnum ResolveSmartCardOption(string serialNumber)
        {
            switch (serialNumber[5])
            {
                case 'A':
                    return SmartCardOptionEnum.ContactContactlessSmartCardEncoders;

                case 'E':
                    return SmartCardOptionEnum.ContactSmartCardEncoder;

                case '0':
                default:
                    return SmartCardOptionEnum.NoSmartCardEncoder;
            }
        }

        public PrinterTypeEnum ResolvePrinterTypeOption(string serialNumber)
        {
            if (serialNumber[3] == '2')
                return PrinterTypeEnum.DualSided;
            return PrinterTypeEnum.SingleSided;
        }
    }
}