﻿namespace Zebra.ZXP.Series13
{
    public class ErrorCode
    {
        public int Code { get; set; }

        public string Description { get; set; }

        public string PossibleCause { get; set; }

        public ErrorCode()
        {
        }

        public ErrorCode(int code, string description)
        {
            Code = code;
            Description = description;
        }

        public ErrorCode(int code, string description, string possibleCause)
        {
            Code = code;
            Description = description;
            PossibleCause = possibleCause;
        }
    }
}