﻿using System;
using System.Collections.Generic;

namespace Zebra.ZXP.Series13
{
    public static class ErrorCodeHelper
    {
        public static List<ErrorCode> ErrorCodes;

        private static void AddErrorCode(int code, string description)
        {
            ErrorCodes.Add(new ErrorCode(code, description));
        }

        private static void AddErrorCode(int code, string description, string possibleCause)
        {
            ErrorCodes.Add(new ErrorCode(code, description, possibleCause));
        }

        private static void InitializeErrorCodes()
        {
            ErrorCodes = new List<ErrorCode>();

            #region ZXP Series 3 Specific Error Codes

            AddErrorCode(-1, "ZBR_ERROR_PRINTER_MECHANICAL_ERROR", "Mechanical error");
            AddErrorCode(1, "ZBR_ERROR_BROKEN_RIBBON", "Indicates a broken ribbon");
            AddErrorCode(2, "ZBR_ERROR_TEMPERATURE", "Print head temperature is too high");
            AddErrorCode(3, "ZBR_ERROR_MECHANICAL_ERROR", "Mechanical error");
            AddErrorCode(4, "ZBR_ERROR_OUT_OF_CARD", "Printer is out of cards, or unable to feed the card");
            AddErrorCode(5, "ZBR_ERROR_CARD_IN_ENCODER", "Unable to encode magnetic or smart card encoder");
            AddErrorCode(6, "ZBR_ERROR_CARD_NOT_IN_ENCODER", "Unable to encode the card because it is not in the encoder");
            AddErrorCode(7, "ZBR_ERROR_PRINT_HEAD_OPEN", "Print head is up");
            AddErrorCode(8, "ZBR_ERROR_OUT_OF_RIBBON", "Out of ribbon");
            AddErrorCode(9, "ZBR_ERROR_REMOVE_RIBBON", "Ribbon needs to be removed");
            AddErrorCode(10, "ZBR_ERROR_PARAMETERS_ERROR", "Wrong number of parameters or a value is incorrect");
            AddErrorCode(11, "ZBR_ERROR_INVALID_COORDINATES", "Invalid coordinates while trying to draw a barcode or graphics");
            AddErrorCode(12, "ZBR_ERROR_UNKNOWN_BARCODE", "Undefined barcode type");
            AddErrorCode(13, "ZBR_ERROR_UNKNOWN_TEXT", "Text for magnetic encoding or bar code drawing is invalid");
            AddErrorCode(14, "ZBR_ERROR_COMMAND_ERROR", "Invalid command");
            AddErrorCode(20, "ZBR_ERROR_BARCODE_DATA_SYNTAX", "Syntax error in the barcode command or parameters");
            AddErrorCode(21, "ZBR_ERROR_TEXT_DATA_SYNTAX", "General text data error");
            AddErrorCode(22, "ZBR_ERROR_GRAPHIC_DATA_SYNTAX", "Syntax error in the graphic command data");
            AddErrorCode(30, "ZBR_ERROR_GRAPHIC_IMAGE_INITIALIZATION", "Unable to initialize the graphics buffer");
            AddErrorCode(31, "ZBR_ERROR_GRAPHIC_IMAGE_MAXIMUM_WIDTH_EXCEEDED", "Graphic object to be drawn exceeds the X range");
            AddErrorCode(32, "ZBR_ERROR_GRAPHIC_IMAGE_MAXIMUM_HEIGHT_EXCEEDED", "Graphic object to be drawn exceeds the Y range");
            AddErrorCode(33, "ZBR_ERROR_GRAPHIC_IMAGE_DATA_CHECKSUM_ERROR", "Graphic data checksum error");
            AddErrorCode(34, "ZBR_ERROR_DATA_TRANSFER_TIME_OUT", "Data time-out error, usually happens when the USB cable is taken out while printing");
            AddErrorCode(35, "ZBR_ERROR_CHECK_RIBBON", "Incorrect ribbon installed");
            AddErrorCode(40, "ZBR_ERROR_INVALID_MAGNETIC_DATA", "Invalid magnetic encoding data");
            AddErrorCode(41, "ZBR_ERROR_MAG_ENCODER_WRITE", "Error while encoding a magnetic stripe");
            AddErrorCode(42, "ZBR_ERROR_READING_ERROR", "Error while reading a magnetic stripe");
            AddErrorCode(43, "ZBR_ERROR_MAG_ENCODER_MECHANICAL", "Magnetic encoder mechanical error");
            AddErrorCode(44, "ZBR_ERROR_MAG_ENCODER_NOT_RESPONDING", "Magnetic encoder not responding");
            AddErrorCode(45, "ZBR_ERROR_MAG_ENCODER_MISSING_OR_CARD_JAM", "Magnetic encoder is missing or the card is jammed before reaching the encoder");
            AddErrorCode(47, "ZBR_ERROR_ROTATION_ERROR", "Error while trying to flip the card");
            AddErrorCode(48, "ZBR_ERROR_COVER_OPEN", "Feeder Cover Lid is open (P110 and P120 only)");
            AddErrorCode(49, "ZBR_ERROR_ENCODING_ERROR", "Error while trying to encode on a magnetic stripe");
            AddErrorCode(50, "ZBR_ERROR_MAGNETIC_ERROR", "Magnetic encoder error");
            AddErrorCode(51, "ZBR_ERROR_BLANK_TRACK", "One or more of the tracks of the magnetic stripe are blank");
            AddErrorCode(52, "ZBR_ERROR_FLASH_ERROR", "Flash memory error");
            AddErrorCode(53, "ZBR_ERROR_NO_ACCESS", "Cannot access the printer");
            AddErrorCode(54, "ZBR_ERROR_SEQUENCE_ERROR", "Reception timeout, protocol errors");
            AddErrorCode(55, "ZBR_ERROR_PROX_ERROR", "Reception timeout, protocol errors");
            AddErrorCode(56, "ZBR_ERROR_CONTACT_DATA_ERROR", "Parameter error");
            AddErrorCode(57, "ZBR_ERROR_PROX_DATA_ERROR", "Parameter error");

            #endregion ZXP Series 3 Specific Error Codes

            #region General SDK Error Codes

            AddErrorCode(60, "ZBR_SDK_ERROR_PRINTER_NOT_SUPPORTED", "Printer not supported");
            AddErrorCode(61, "ZBR_SDK_ERROR_CANNOT_GET_PRINTER_HANDLE", "Unable to open handle to Zebra printer driver");
            AddErrorCode(62, "ZBR_SDK_ERROR_CANNOT_GET_PRINTER_DRIVER", "Cannot open printer driver");
            AddErrorCode(63, "ZBR_SDK_ERROR_INVALID_PARAMETER", "One of the arguments is invalid");
            AddErrorCode(64, "ZBR_SDK_ERROR_PRINTER_BUSY", "Printer is in use");
            AddErrorCode(65, "ZBR_SDK_ERROR_INVALID_PRINTER_HANDLE", "Invalid printer handle");
            AddErrorCode(66, "ZBR_SDK_ERROR_CLOSE_HANDLE_ERROR", "Error closing printer driver handle");
            AddErrorCode(67, "ZBR_SDK_ERROR_COMMUNICATION_ERROR", "Command failed due to communication error");
            AddErrorCode(68, "ZBR_SDK_ERROR_BUFFER_OVERFLOW", "Response too large for buffer");
            AddErrorCode(69, "ZBR_SDK_ERROR_READ_DATA_ERROR", "Error reading data");
            AddErrorCode(70, "ZBR_SDK_ERROR_WRITE_DATA_ERROR", "Error writing data");
            AddErrorCode(71, "ZBR_SDK_ERROR_LOAD_LIBRARY_ERROR", "Error loading SDK");
            AddErrorCode(72, "ZBR_SDK_ERROR_INVALID_STRUCT_ALIGNMENT", "Invalid structure alignment");
            AddErrorCode(73, "ZBR_SDK_ERROR_GETTING_DEVICE_CONTEXT", "Unable to create the device context for the driver");
            AddErrorCode(74, "ZBR_SDK_ERROR_SPOOLER_ERROR", "Print spooler error");
            AddErrorCode(75, "ZBR_SDK_ERROR_OUT_OF_MEMORY", "Operating system is out of memory");
            AddErrorCode(76, "ZBR_SDK_ERROR_OUT_OF_DISK_SPACE", "Operating system is out of disk space");
            AddErrorCode(77, "ZBR_SDK_ERROR_USER_ABORT", "Print job aborted by the user");
            AddErrorCode(78, "ZBR_SDK_ERROR_APPLICATION_ABORT", "Application aborted");
            AddErrorCode(79, "ZBR_SDK_ERROR_CREATE_FILE_ERROR", "Error creating file");
            AddErrorCode(80, "ZBR_SDK_ERROR_WRITE_FILE_ERROR", "Error writing file");
            AddErrorCode(81, "ZBR_SDK_ERROR_READ_FILE_ERROR", "Error reading file");
            AddErrorCode(82, "ZBR_SDK_ERROR_INVALID_MEDIA", "Invalid media");
            AddErrorCode(83, "ZBR_SDK_ERROR_MEMORY_ALLOCATION", "Insufficient system resources to perform necessary memory allocation");
            AddErrorCode(255, "ZBR_SDK_ERROR_UNKNOWN_ERROR", "An unknown but serious exception has occurred");

            #endregion General SDK Error Codes

            #region Graphic Error Codes

            AddErrorCode(8001, "ZBR_GDI_ERROR_GENERIC_ERROR", "Window API error, call GetLastError() function from Win32 API for error information");
            AddErrorCode(8002, "ZBR_GDI_ERROR_INVALID_PARAMETER", "One of the arguments is invalid");
            AddErrorCode(8003, "ZBR_GDI_ERROR_OUT_OF_MEMORY", "Operating system is out of memory");
            AddErrorCode(8004, "ZBR_GDI_ERROR_OBJECT_BUSY", "One of the objects specified in the API call is in use");
            AddErrorCode(8005, "ZBR_GDI_ERROR_INSUFFICIENT_BUFFER", "A buffer specified as an argument in the API call is not large enough");
            AddErrorCode(8006, "ZBR_GDI_ERROR_NOT_IMPLEMENTED", "Method is not implemented");
            AddErrorCode(8007, "ZBR_GDI_ERROR_WIN32_ERROR", "Method generated a Win32 error, call GetLastError() function from Win32 API for error information");
            AddErrorCode(8008, "ZBR_GDI_ERROR_WRONG_STATE", "Object called by the API is in an invalid state");
            AddErrorCode(8009, "ZBR_GDI_ERROR_ABORTED", "Method aborted");
            AddErrorCode(8010, "ZBR_GDI_ERROR_FILE_NOT_FOUND", "File not found");
            AddErrorCode(8011, "ZBR_GDI_ERROR_VALUE_OVERFLOW", "Arithmetic operation in the method caused a numeric overflow");
            AddErrorCode(8012, "ZBR_GDI_ERROR_ACCESS_DENIED", "Access denied to the specified file");
            AddErrorCode(8013, "ZBR_GDI_ERROR_UNKNOWN_IMAGE_FORMAT", "Specified image file format is unknown");
            AddErrorCode(8014, "ZBR_GDI_ERROR_FONT_FAMILY_NOT_FOUND", "Specified font is not installed");
            AddErrorCode(8015, "ZBR_GDI_ERROR_FONT_STYLE_NOT_FOUND", "Invalid font style");
            AddErrorCode(8016, "ZBR_GDI_ERROR_NOT_TRUE_TYPE_FONT", "Specified font is not a True Type font and cannot be used with GDI+");
            AddErrorCode(8017, "ZBR_GDI_ERROR_UNSUPPORTED_GDIPLUS_VERSION", "Installed GDI+ version");
            AddErrorCode(8018, "ZBR_GDI_ERROR_GDIPLUS_NOT_INITIALIZED", "The GDI+ API is not initialized");
            AddErrorCode(8019, "ZBR_GDI_ERROR_PROPERTY_NOT_FOUND", "Specified property does not exist in the image");
            AddErrorCode(8020, "ZBR_GDI_ERROR_PROPERTY_NOT_SUPPORTED", "Specified property is not supported by the image format");
            AddErrorCode(8021, "ZBR_GDI_ERROR_GRAPHICS_ALREADY_INITIALIZED", "Graphic buffer has already been initialized");
            AddErrorCode(8022, "ZBR_GDI_ERROR_NO_GRAPHIC_DATA", "No data in the graphic buffer to print");
            AddErrorCode(8023, "ZBR_GDI_ERROR_GRAPHICS_NOT_INITIALIZED", "Graphics buffer has not been initialized");
            AddErrorCode(8024, "ZBR_GDI_ERROR_GETTING_DEVICE_CONTEXT", "Unable to create the device context for the driver");
            AddErrorCode(8025, "ZBR_DLG_ERROR_DLG_CANCELED", "User closed or canceled the DLG window");
            AddErrorCode(8026, "ZBR_DLG_ERROR_SETUP_FAILURE", "PrintDlg function failed to load the required resources");
            AddErrorCode(8027, "ZBR_DLG_ERROR_PARSE_FAILURE", "PrintDlg function failed to parse the strings in the [devices] section of the WIN.INI file");
            AddErrorCode(8028, "ZBR_DLG_ERROR_RET_DEFAULT_FAILURE", "PD_RETURNDEFAULT flag was specified in the Flags member of the PRINTDLG structure, but the hDevMode or hDevNames member was not NULL");
            AddErrorCode(8029, "ZBR_DLG_ERROR_LOAD_DRV_FAILURE", "PrintDlg function failed to load the device driver for the specified printer");
            AddErrorCode(8030, "ZBR_DLG_ERROR_GET_DEVMODE_FAIL", "Printer driver failed to initialize a DEVMODE structure");
            AddErrorCode(8031, "ZBR_DLG_ERROR_INIT_FAILURE", "PrintDlg function failed during initialization, and there is no more specific extended error code to describe the failure");
            AddErrorCode(8032, "ZBR_DLG_ERROR_NO_DEVICES", "No printer drivers were found");
            AddErrorCode(8033, "ZBR_DLG_ERROR_NO_DEFAULT_PRINTER", "A default printer does not exist");
            AddErrorCode(8034, "ZBR_DLG_ERROR_DN_DM_MISMATCH", "Data in the DEVMODE and DEVNAMES structures describes two different printers");
            AddErrorCode(8035, "ZBR_DLG_ERROR_CREATE_IC_FAILURE", "PrintDlg function failed when it attempted to create an information context");
            AddErrorCode(8036, "ZBR_DLG_ERROR_PRINTER_NOT_FOUND", "The [devices] section of the WIN.INI file did not contain an entry for the requested printer");
            AddErrorCode(8037, "ZBR_DLG_ERROR_DEFAULT_DIFFERENT", "Error occurs when you store the DEVNAMES structure, and the user changes the default printer by using the Control Panel");

            #endregion Graphic Error Codes

            #region Ethernet SmartCard Encoding SDK Error Codes

            AddErrorCode(65001, "Device not open");
            AddErrorCode(65002, "Device already open");
            AddErrorCode(65003, "Device not available");
            AddErrorCode(65008, "Invalid argument");
            AddErrorCode(65012, "Buffer overflow");
            AddErrorCode(65021, "Memory allocation error");
            AddErrorCode(65022, "No devices found");
            AddErrorCode(65023, "Disconnect error");
            AddErrorCode(65035, "Unexpected error");

            #endregion Ethernet SmartCard Encoding SDK Error Codes
        }

        public static ErrorCode GetErrorCode(int code)
        {
            if (ErrorCodes == null)
                InitializeErrorCodes();
            foreach (var errorCode in ErrorCodes)
            {
                if (errorCode.Code == code)
                    return errorCode;
            }
            throw new Exception("Unknown error code");
        }
    }
}