﻿using System;

namespace Zebra.ZXP.Series13
{
    [AttributeUsage(AttributeTargets.Property)]
    internal class OperationalParameterIndex
        : Attribute
    {
        public int Index { get; private set; }

        public OperationalParameterIndex(int index)
        {
            this.Index = index;
        }
    }
}