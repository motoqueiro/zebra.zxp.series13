using System;
using System.Runtime.InteropServices;
using Zebra.ZXP.Series13.Enums;

namespace Zebra.ZXP.Series13
{
    public class ZBRPrinter
    {
        #region SDK Specific Functions

        /// <summary>
        /// Returns the SDK dll version
        /// </summary>
        /// <param name="major">Pointer to major version number</param>
        /// <param name="minor">Pointer to minor version number</param>
        /// <param name="engLevel">Pointer to engineering level version number</param>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetSDKVer", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern void ZBRPRNGetSDKVer(out int major, out int minor, out int engLevel);

        /// <summary>
        /// Returns the SDK dll version
        /// </summary>
        /// <param name="major">Pointer to major version number</param>
        /// <param name="minor">Pointer to minor version number</param>
        /// <param name="engLevel">Pointer to engineering level version number</param>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetSDKVsn", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern void ZBRPRNGetSDKVsn(out int major, out int minor, out int engLevel);

        /// <summary>
        /// Returns the SDK's product version
        /// </summary>
        /// <param name="productVersion">Buffer to hold returned product version</param>
        /// <param name="buffSize">Size of the returned buffer in bytes</param>
        /// <returns></returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetSDKProductVer", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetSDKProductVer(out byte[] productVersion, out int buffSize);

        /// <summary>
        /// Gets a handle for a printer driver
        /// </summary>
        /// <param name="handle">Pointer to returned printer driver handle</param>
        /// <param name="driverName">Pointer to printer driver name</param>
        /// <param name="printerType">Pointer to returned printer type</param>
        /// <param name="errorCode"></param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRGetHandle", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern int ZBRGetHandle(out IntPtr handle, byte[] driverName, out int printerType, out int errorCode);

        /// <summary>
        /// Closes a handle for a printer driver
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRCloseHandle", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern int ZBRCloseHandle(IntPtr handle, out int errorCode);

        /// <summary>
        /// Queries the printer driver for current state of status message suppression
        /// </summary>
        /// <param name="hPrinter">Handle to printer driver</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="state">State of status message suppression: -1 = unknown; 0 = suppression disabled; 1 = suppression enabled</param>
        /// <param name="errorCode">Error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        /// <remarks>Call this function before sending the first job to the printer to determine the state of status message suppression if a specific suppression state is required</remarks>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetMsgSuppressionState", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetMsgSuppressionState(IntPtr hPrinter, int printerType, out int state, out int errorCode);

        /// <summary>
        /// Sets the status message suppression state for the printer driver
        /// </summary>
        /// <param name="handle">Handle to printer driver</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="suppressMessages">Status message suppression state</param>
        /// <param name="errorCode">Pointer to error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        /// <remarks>This function needs to be called only once if the state of status message suppression does not need to be changed. Once set, the printer driver will retain the setting</remarks>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSuppressStatusMsgs", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSuppressStatusMsgs(IntPtr handle, int printerType, int suppressMessages, out int errorCode);

        /// <summary>
        /// Defines the type of overlay mode and the bitmap overlay image to be used for printing. The printer driver will use this information during the printing process
        /// </summary>
        /// <param name="handle">Handle to printer driver</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="side">Defines the side of the card to apply the overlay 0 = front, 1 = back</param>
        /// <param name="overlay">Structure defining the type of overlay</param>
        /// <param name="filename">Filename and path of the bitmap overlay image</param>
        /// <param name="errorCode">Pointer to error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetOverlayMode", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        public static extern int ZBRPRNSetOverlayMode(IntPtr handle, int printerType, int side, OverlayTypeEnum overlay, [MarshalAs(UnmanagedType.LPStr)] string filename, out int errorCode);

        #endregion SDK Specific Functions

        #region Printer Command Functions

        /// <summary>
        /// Prints an *.prn file
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="filename">Pointer to *.prn filename</param>
        /// <param name="errorCode">Pointer to error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNPrintPrnFile", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNPrintPrnFile(IntPtr handle, int printerType, byte[] filename, out int errorCode);

        /// <summary>
        /// Queries the printer for its current status
        /// </summary>
        /// <param name="handle">Handle to printer driver</param>
        /// <param name="printerType">Printer type value</param>
        /// <returns>0 - Printer is not in error state or >0 - Error code</returns>
        /// <remarks>Call this function prior to sending a job to the printer to determine if printer is able to perform a job</remarks>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNCheckForErrors", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNCheckForErrors(IntPtr handle, int printerType);

        /// <summary>
        /// Clears the error status line
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNClrErrStatusLn", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNClrErrStatusLn(IntPtr handle, int printerType, out int errorCode);

        #endregion Printer Command Functions

        /// <summary>
        /// Gets the total number of cards printed
        /// </summary>
        /// <param name="handle"><param name="handle">Handle to printer driver</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="printCount">Pointer to total card count</param>
        /// <param name="errorCode">Pointer to error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetPrintCount", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetPrintCount(IntPtr handle, int printerType, out int printCount, out int errorCode);

        /// <summary>
        /// Queries the printer driver for current job status
        /// </summary>
        /// <param name="handle"><param name="handle">Handle to printer driver</param>
        /// <param name="printerType">Printer type value</param>
        /// <returns>0 - Current job is proceeding without error or >0 - error code</returns>
        /// <remarks>Call this function after sending a job/while job is active to the printer to determine if job is being performed or has encountered an error</remarks>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetPrintStatus", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetPrintStatus(IntPtr handle, int printerType);

        /// <summary>
        /// Returns the number of ribbon panels remaining
        /// </summary>
        /// <param name="handle"><param name="handle">Handle to printer driver</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="panels">Number of panels available for printing</param>
        /// <param name="errorCode">Pointer to error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetPanelsRemaining", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetPanelsRemaining(IntPtr handle, int printerType, out int panels, out int errorCode);

        /// <summary>
        /// Returns the number of ribbon panels that have been printed
        /// </summary>
        /// <param name="handle"><param name="handle">Handle to printer driver</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="panels">Number of panels available for printing</param>
        /// <param name="errorCode">Pointer to error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetPanelsPrinted", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetPanelsPrinted(IntPtr handle, int printerType, out int panels, out int errorCode);

        /// <summary>
        /// Retrieves the printer serial number
        /// </summary>
        /// <param name="handle"><param name="handle">Handle to printer driver</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="serialNumber">Pointer to serial number buffer</param>
        /// <param name="responseSize">Pointer to buffer size</param>
        /// <param name="errorCode">Pointer to error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetPrinterSerialNumber", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetPrinterSerialNumber(IntPtr handle, int printerType, byte[] serialNumber, out int responseSize, out int errorCode);

        /// <summary>
        /// Retrieves the printer serial number
        /// </summary>
        /// <param name="handle">Handle to printer driver</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="serialNumber">Pointer to serial number buffer</param>
        /// <param name="responseSize">Pointer to buffer size</param>
        /// <param name="errorCode">Pointer to error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetPrinterSerialNumb", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetPrinterSerialNumb(IntPtr handle, int printerType, byte[] serialNum, out int responseSize, out int errorCode);

        /// <summary>
        /// Retrieves the printer options
        /// </summary>
        /// <param name="handle">Handle to printer driver</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="options">Pointer to options buffer:
        /// <list type="bullet">
        ///     <item>
        ///         <term>Base Unit Designation:</term>
        ///         <description>
        ///             <list type="bullet">
        ///                 <item>
        ///                     <term>1</term>
        ///                     <description>Single-sided printing</description>
        ///                 </item>
        ///                 <item>
        ///                     <term>2</term>
        ///                     <description>Dual-sided printing</description>
        ///                 </item>
        ///             </list>
        ///         </description>
        ///     </item>
        ///     <item>
        ///         <term>SmartCard option:</term>
        ///         <description>
        ///             <list type="bullet">
        ///                 <item>
        ///                     <term>0</term>
        ///                     <description>No SmartCard (SC) encoder</description>
        ///                 </item>
        ///                 <item>
        ///                     <term>A</term>
        ///                     <description>Contact & Contactless SC encoders</description>
        ///                 </item>
        ///                 <item>
        ///                     <term>E</term>
        ///                     <description>Contact station SC encoder</description>
        ///                 </item>
        ///             </list>
        ///         </description>
        ///     </item>
        ///     <item>
        ///         <term>Magnetic encoder option:</term>
        ///         <description>
        ///             <list type="bullet">
        ///                 <item>
        ///                     <term>0</term>
        ///                     <description>No magnetic encoder</description>
        ///                 <item>
        ///                     <term>M</term>
        ///                     <description>Magnetic encoder</description>
        ///                 </item>
        ///             </list>
        ///         </description>
        ///     </item>
        ///     <item>
        ///         <term>Security option:</term>
        ///         <description>
        ///             <list type="bullet">
        ///                 <item>
        ///                     <term>0</term>
        ///                     <description>No enclosure locks</description>
        ///                 </item>
        ///                 <item>
        ///                     <term>A</term>
        ///                     <description>Enclosure locks</description>
        ///                 </item>
        ///                 <item>
        ///                     <term></term>
        ///                     <description></description>
        ///                 </item>
        ///             </list>
        ///         </description>
        ///     </item>
        ///     <item>
        ///         <term>Interface:</term>
        ///         <description>
        ///             <list type="bullet">
        ///                 <item>
        ///                     <term>0</term>
        ///                     <description>USB only</description>
        ///                 </item>
        ///                 <item>
        ///                     <term>C</term>
        ///                     <description>USB & Ethernet</description>
        ///                 </item>
        ///             </list>
        ///         </description>
        ///     </item>
        ///     <item>
        ///         <term>V</term>
        ///         <description>Firmware version</description>
        ///     </item>
        /// </list>
        /// <param name="responseSize">pointer to response size</param>
        /// <param name="errorCode">Pointer to error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetPrinterOptions", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetPrinterOptions(IntPtr handle, int printerType, byte[] options, out int responseSize, out int errorCode);

        /// <summary>
        /// Retrieves the print head serial number
        /// </summary>
        /// <param name="handle">Handle to printer driver</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="serialNumber">Pointer to serial number buffer</param>
        /// <param name="responseSize">Pointer to response size</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetPrintHeadSerialNumber", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetPrintHeadSerialNumber(IntPtr handle, int printerType, byte[] serialNumber, out int responseSize, out int errorCode);

        /// <summary>
        /// Retrieves the print head serial number
        /// </summary>
        /// <param name="handle">Handle to printer driver</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="serialNumber">Pointer to serial number buffer</param>
        /// <param name="responseSize">Pointer to response size</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetPrintHeadSerialNumb", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetPrintHeadSerialNumb(IntPtr handle, int printerType, byte[] serialNumber, out int responseSize, out int errorCode);

        /// <summary>
        /// Retrieves the operational parameters
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="parameterIndex">Requested parameter index</param>
        /// <param name="operationalParameter">Pointer to operational parameter buffer</param>
        /// <param name="responseSize">Pointer to response size</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetOpParam", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetOpParam(IntPtr handle, int printerType, int parameterIndex, byte[] operationalParameter, out int responseSize, out int errorCode);

        /// <summary>
        /// Returns the printer's current status code
        /// </summary>
        /// <param name="statusCode">Pointer to current error code status</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetPrinterStatus", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetPrinterStatus(out int statusCode);

        /// <summary>
        /// Retrieves the state of various sensors for the printer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="status">Pointer to sensor status bytebit 0 - Main Door:
        /*0 = closed
        1 = open
        bit 1 - Ribbon Sensor:
        0 = transparent/no panel
        1 = opaque panel
        bit 2 - ATM Sensor:
        0 = clear
        1 = blocked
        bit 3 - Print Synchro Sensor:
        0 = clear
        1 = blocked
        bit 4 - Feeder Door:
        0 = closed
        1 = open
        bit 5 - Flipper Position:
        0 = card feed position
        1 = card print position
        bit 6 - Card In Flipper:
        0 = no card
        1 = card
        bit 7 - Mag Synchro Sensor:
        0 = clear
        1 = blocked</param>*/

        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetSensorStatus", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetSensorStatus(IntPtr handle, int printerType, out byte status, out int errorCode);

        /// <summary>
        /// Queries the print driver to determine if the printer is currently executing a print job
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - printer is ready or  0 - printer is currently executing a print job</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNIsPrinterReady", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNIsPrinterReady(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Reports current values for the Printing, Cleaning, and Cleaning Pass counters
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="imageCounter">Pointer to total number of head-down image passes made by printer since new (note that each ribbon panel used counts as a pass)</param>
        /// <param name="cleanCounter">Pointer to current setting for image passes that trigger a cleaning alert</param>
        /// <param name="cleanCardCounter">Pointer to current setting for passes performed using a Cleaning Card</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNChkDueForCleaning", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNChkDueForCleaning(IntPtr handle, int printerType, out int imageCounter, out int cleanCounter, out int cleanCardCounter, out int errorCode);

        /// <summary>
        /// Starts a cleaning sequence
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNStartCleaningSeq", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNStartCleaningSeq(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Starts card cleaning sequence
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNStartCleaningCardSeq", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNStartCleaningCardSeq(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Gets cleaning values
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="imageCounter">Pointer to total number of head-down Image passes(each ribbon panel used counts as a pass)</param>
        /// <param name="cleanCounter">Pointer to image passes before a cleaning alert is sent, default = 5000</param>
        /// <param name="cleanCardCounter">Pointer to the number of cleaning card passes when cleaning, default = 5</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetCleaningParam", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetCleaningParam(IntPtr handle, int printerType, out int imageCounter, out int cleanCounter, out int cleanCardCounter, out int errorCode);

        /// <summary>
        /// Sets the cleaning parameters
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="ribbonPanelCounter">Number of panels printed before start cleaning, default = 5000</param>
        /// <param name="cleanCardPass">Number of cleaning passes through printer, default = 5</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetCleaningParam", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetCleaningParam(IntPtr handle, int printerType, int ribbonPanelCounter, int cleanCardPass, out int errorCode);

        #region Printer Setup Functions

        /// <summary>
        /// Resets printer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNResetPrinter", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNResetPrinter(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Initiates a printer self-adjust sequence
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSelfAdj", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSelfAdj(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        ///
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="checksum">Pointer to returned checksum</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetChecksum", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetChecksum(IntPtr handle, int printerType, out int checksum, out int errorCode);

        /// <summary>
        /// Sets the card feeding mode
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="mode">Printer with or without card feeder</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetCardFeedingMode", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetCardFeedingMode(IntPtr handle, int printerType, int mode, out int errorCode);

        /// <summary>
        /// Set a printer's print head resistance
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="resistance">Print head resistance value in ohms</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <remarks>This function would be used following replacement of a printer's printhead. The initial value to be used is the printhead resistance displayed on the printhead's label. If adjustments to the initial value are required, do not exceed � 200 from the label's value</remarks>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetPrintHeadResistance", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetPrintHeadResistance(IntPtr handle, int printerType, int resistance, out int errorCode);

        /// <summary>
        /// Clears a printer's media path
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNClrMediaPath", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNClrMediaPath(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Perfoms an immediate save of parameters to flash memory
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNImmediateParamSave", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNImmediateParamSave(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Sets the X-axis Print Origin plus or minus the offset value from the current setting. Note: offset value is in dots
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="offset">Offset value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <remarks>300 dots per inch</remarks>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetRelativeXOffset", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetRelativeXOffset(IntPtr handle, int printerType, int offset, out int errorCode);

        /// <summary>
        /// Sets the Y-axis Print Origin plus or minus the offset value from the current setting. Note: offset value is in dots
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="offset">Offset value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <remarks>300 dots per inch</remarks>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetRelativeYOffset", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetRelativeYOffset(IntPtr handle, int printerType, int offset, out int errorCode);

        /// <summary>
        /// Sets the horizontal (X-axis) start print offset point
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="offset">Offset value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <remarks>300 dots per inch</remarks>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetStartPrintXOffset", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetStartPrintXOffset(IntPtr handle, int printerType, int offset, out int errorCode);

        /// <summary>
        /// Sets the horizontal (Y-axis) start print offset point
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="offset">Offset value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <remarks>300 dots per inch</remarks>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetStartPrintYOffset", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetStartPrintYOffset(IntPtr handle, int printerType, int offset, out int errorCode);

        /// <summary>
        /// Alters the Horizontal (X-axis) or Vertical (Y-axis) Start Print Offset Point in dots. Note: This function is supported by dual-sided printers only.
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="x_y">X or Y axis</param>
        /// <param name="offset">Offset value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <remarks>300 dots per inch</remarks>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetStartPrintSideBOffset", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetStartPrintSideBOffset(IntPtr handle, int printerType, int x_y, int offset, out int errorCode);

        /// <summary>
        /// Sets the offset for card side B X-axis start print point. Note: This function is supported by dual-sided printers only
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="offset">Offset value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <remarks>300 dots per inch</remarks>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetStartPrintSideBXOffset", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetStartPrintSideBXOffset(IntPtr handle, int printerType, int offset, out int errorCode);

        /// <summary>
        /// Sets the card side B Y-axis start print offset point. Note: This function is supported by dual-sided printers only
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="offset">Offset value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <remarks>300 dots per inch</remarks>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetStartPrintSideBYOffset", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetStartPrintSideBYOffset(IntPtr handle, int printerType, int offset, out int errorCode);

        /// <summary>
        /// Retrieves the IP Address for the specified Ethernet-connected printer
        /// </summary>
        /// <param name="printerName">Printer name assigned by printer driver</param>
        /// <param name="ipAddress">Printer's IP Address</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNGetIPAddress", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNGetIPAddress([MarshalAs(UnmanagedType.LPStr)] string printerName, out byte[] ipAddress);

        #endregion Printer Setup Functions

        #region Image Buffer Functions

        /// <summary>
        /// Sets the color contrast level for the specified image buffer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="imgageBufferIndex">Image buffer index</param>
        /// <param name="contrast">Contrast value (0 thru 10)</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetColorContrast", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetColorContrast(IntPtr handle, int printerType, int imgageBufferIndex, int contrast, out int errorCode);

        /// <summary>
        /// Sets the color intensity level for the specified image buffer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="imgageBufferIndex">Image buffer index</param>
        /// <param name="contrast">Contrast value (0 thru 10)</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetContrastIntensityLVl", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetContrastIntensityLvl(IntPtr handle, int printerType, int imageBufferIndex, int intensity, out int errorCode);

        /// <summary>
        /// Sets the hologram intensity level
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="intensity">Intensity value (0 thru 10)</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetHologramIntensity", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetHologramIntensity(IntPtr handle, int printerType, int intensity, out int errorCode);

        /// <summary>
        /// Sets the monochrome contrast level
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="contrast">Contrast value (0 thru 10)</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetMonoContrast", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetMonoContrast(IntPtr handle, int printerType, int contrast, out int errorCode);

        /// <summary>
        ///Sets the monochrome ribbon transfer intensity level
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="intensity">Intensity value (0 thru 10)</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetMonoIntensity", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetMonoIntensity(IntPtr handle, int printerType, int intensity, out int errorCode);

        /// <summary>
        /// Clears the specified color buffer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="colorBufferIndex">Buffer to clear</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNClrSpecifiedBmp", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNClrSpecifiedBmp(IntPtr handle, int printerType, int colorBufferIndex, out int errorCode);

        /// <summary>
        /// Clears the monochrome image buffer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="clearVarnish">Clear varnish</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNClrMonoImgBuf", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNClrMonoImgBuf(IntPtr handle, int printerType, int clearVarnish, out int errorCode);

        /// <summary>
        /// Clears the monochrome image buffers
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="clearBuffer"></param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNClrMonoImgBufs", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNClrMonoImgBufs(IntPtr handle, int printerType, int clearBuffer, out int errorCode);

        /// <summary>
        /// Clears all of the color image buffers
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNClrColorImgBufs", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNClrColorImgBufs(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Clears the specified color buffer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="colorBufferIndex">Index to the color buffer</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNClrColorImgBuf", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNClrColorImgBuf(IntPtr handle, int printerType, int colorBufferIndex, out int errorCode);

        /// <summary>
        /// Prints the monochrome buffer and ejects the card
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNPrintMonoImgBuf", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNPrintMonoImgBuf(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Prints the monochrome buffer and moves the card as directed
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="cardCommand">Post-print card command</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNPrintMonoImgBufEx", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNPrintMonoImgBufEx(IntPtr handle, int printerType, int cardCommand, out int errorCode);

        /// <summary>
        /// Print the specified color image buffer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="imageBufferIndex">Color image buffer index</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNPrintColorImgBuf", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNPrintColorImgBuf(IntPtr handle, int printerType, int imageBufferIndex, out int errorCode);

        /// <summary>
        /// Prints with clear varnish from all Image Buffers and ejects the card
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNPrintClearVarnish", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNPrintClearVarnish(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Print with clear varnish
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNPrintVarnish", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNPrintVarnish(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Print with clear varnish and moves the card as directed
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="printCardCommand">Post-print card command value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNPrintVarnishEx", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNPrintVarnishEx(IntPtr handle, int printerType, int printCardCommand, out int errorCode);

        /// <summary>
        ///
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="printCardCommand">Post-print card command value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNPrintHologramOverlay", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNPrintHologramOverlay(IntPtr handle, int printerType, int printCardCommand, out int errorCode);

        /// <summary>
        /// Prints from a selected color dye sublimation ribbon panel (Yellow, Magenta, Cyan, or Black) using data from an associated image buffer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="imageBufferIndex">Color image buffer number</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNPrintCardPanel", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNPrintCardPanel(IntPtr handle, int printerType, int imageBufferIndex, out int errorCode);

        /// <summary>
        /// Prints the monochrome buffer and ejects the card
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNPrintMonoPanel", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNPrintMonoPanel(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Draws a transparent rectangle in the monochrome image buffer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="startX">Start X position in dots (upper left corner X coordinate)</param>
        /// <param name="startY">start Y position in dots (upper left corner Y coordinate)</param>
        /// <param name="width">Width of the box in dots</param>
        /// <param name="height">Height of the box in dots</param>
        /// <param name="thickness">Line thickness in dots</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNWriteBox", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNWriteBox(IntPtr handle, int printerType, int startX, int startY, int width, int height, int thickness, out int errorCode);

        /// <summary>
        /// Draws a transparent rectangle in the monochrome image buffer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="startX">Start X position in dots (upper left corner X coordinate)</param>
        /// <param name="startY">start Y position in dots (upper left corner Y coordinate)</param>
        /// <param name="width">Width of the box in dots</param>
        /// <param name="height">Height of the box in dots</param>
        /// <param name="thickness">Line thickness in dots</param>
        /// <param name="graphicMode">Graphic mode: 0 =clear print area and load reverse bitmap image | 1 - clear print area and load bitmap image | 2 - merge bit map image with print area</param>
        /// <param name="isVarnish">1 - use varnish overlay | 0 - do not use varnish overlay</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNWriteBoxEx", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNWriteBoxEx(IntPtr handle, int printerType, int startX, int startY, int width, int height, int thickness, int graphicMode, int isVarnish, out int errorCode);

        /// <summary>
        /// Draws a text string in the monochrome image buffer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="startX">Start X position in dots (upper left corner X coordinate)</param>
        /// <param name="startY">start Y position in dots (upper left corner Y coordinate)</param>
        /// <param name="rotation">Rotation: 0 = origin lower left no rotation | 1 = origin lower left 90 degrees | 2 = origin lower left 180 degrees | 3 = origin lower left 270 degrees | 4 = origin center no rotation | 5 = origin center 90 degrees | 6 = origin center 180 degrees | 7 = origin center 270 degrees</param>
        /// <param name="isBold">Use bold or not</param>
        /// <param name="height">Height in dots of the text box: 104 - 28 point normal | 140 - 28 point bold</param>
        /// <param name="text">Pointer to text buffer</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNWriteText", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNWriteText(IntPtr handle, int printerType, int startX, int startY, int rotation, int isBold, int height, byte[] text, out int errorCode);

        /// <summary>
        /// Draws a text string into the monochrome image buffer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="startX">Start X position in dots (upper left corner X coordinate)</param>
        /// <param name="startY">start Y position in dots (upper left corner Y coordinate)</param>
        /// <param name="rotation">Rotation: 0 = origin lower left no rotation | 1 = origin lower left 90 degrees | 2 = origin lower left 180 degrees | 3 = origin lower left 270 degrees | 4 = origin center no rotation | 5 = origin center 90 degrees | 6 = origin center 180 degrees | 7 = origin center 270 degrees</param>
        /// <param name="isBold">Use bold or not</param>
        /// <param name="width">Width of the box in dots</param>
        /// <param name="height">Height of the box in dots</param>
        /// <param name="graphicMode">Graphic modes:
        /// 0 - clear print area and load reverse bitmap image | 1 - clear print area and load bitmap image | 2 - merge bit map image with print area</param>
        /// <param name="graphicMode">Graphic mode: 0 - clear print area and load reverse bitmap image | 1 - clear print area and load bitmap image | 2 - merge bit map image with print area</param>
        /// <param name="text">Pointer to text buffer</param>
        /// <param name="isVarnish">1 - use varnish overlay | 0 - do not use varnish overlay</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNWriteTextEx", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNWriteTextEx(IntPtr handle, int printerType, int startX, int startY, int rotation, int isBold, int widht, int height, int graphicMode, byte[] text, int isVarnish, out int errorCode);

        /// <summary>
        /// Specifies the printing width for the x axis in dots
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="xWidth">End of print x axis in dots</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNSetEndOfPrint", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNSetEndOfPrint(IntPtr handle, int printerType, int xWidth, out int errorCode);

        #endregion Image Buffer Functions

        #region Position Card Functions

        /// <summary>
        /// Moves a card to the print-ready position
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNMovePrintReady", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNMovePrintReady(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Moves the card back to the ready position
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNReversePrintReady", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNReversePrintReady(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Moves the card to the output hopper
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNEjectCard", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNEjectCard(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Flips a card for printing, or magnetic encoding, or SmartCard encoding
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNFlipCard", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNFlipCard(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Moves the card a specified distance
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="steps">Number of steps to move card (Note: 100 steps = 8mm/0.315in), a positive number moves card forward and a negative number moves card backward</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNMoveCard", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNMoveCard(IntPtr handle, int printerType, int steps, out int errorCode);

        /// <summary>
        /// Moves the card backward by specified number of steps
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="steps">Number of steps to move card (Note: 100 steps = 8mm/0.315in), a positive number moves card forward and a negative number moves card backward</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNMoveCardBkwd", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNMoveCardBkwd(IntPtr handle, int printerType, int steps, out int errorCode);

        /// <summary>
        /// Moves the card forward by specified number of steps
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="steps">Number of steps to move card (Note: 100 steps = 8mm/0.315in), a positive number moves card forward and a negative number moves card backward</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNMoveCardFwd", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNMoveCardFwd(IntPtr handle, int printerType, int steps, out int errorCode);

        /// <summary>
        /// Resynchronize the card under the print head
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="steps">Number of steps to move card (Note: 100 steps = 8mm/0.315in), a positive number moves card forward and a negative number moves card backward</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNResync", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNResync(IntPtr handle, int printerType, out int errorCode);

        /// <summary>
        /// Positions a card for SmartCard encoding
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="cardType">Type of card to be encoded: 1 - Contact | 2 - Contactless | 3 - UHF</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNStartSmartCard", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNStartSmartCard(IntPtr handle, int printerType, int cardType, out int errorCode);

        /// <summary>
        /// Moves the SmartCard to the print ready position or ejects the card following a SmartCard encoding operation
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="cardType">Type of card to be encoded: 1 - Contact | 2 - Contactless | 3 - UHF</param>
        /// <param name="moveType">Card movement to be performed: 0 - move card to print ready position | 1 - eject card</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNEndSmartCard", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNEndSmartCard(IntPtr handle, int printerType, int cardType, int moveType, out int errorCode);

        #endregion Position Card Functions

        #region Test Card Functions

        /// <summary>
        /// Prints a test card
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="testCardType">Test card type: 0 - standard test card | 1 - printer test card | 2 - magnetic encoder test card | 3 - lamination test card</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNPrintTestCard", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNPrintTestCard(IntPtr handle, int printerType, int testCardType, out int errorCode);

        #endregion Test Card Functions

        #region Barcode Card Functions

        /// <summary>
        /// Writes a barcode to the monochrome buffer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="startX">Start X position in dots</param>
        /// <param name="startY">Start Y position in dots</param>
        /// <param name="rotation">Rotation: 0 - origin lower left and no rotation | 1 - origin lower left and 90 degrees | 2 - origin lower left and 180 degrees | 3 - origin lower left and 270 degrees | 4 - origin center and no rotation | 5 - origin center and 90 degrees | 6 - origin center and 180 degrees | 7 - origin center and 270 degrees</param>
        /// <param name="barcodeType">Bar code type: 0 = code 39 (3 of 9 alphanumeric) | 1 = 2/5 interleave(numeric, even count) | 2 = 2/5 industrial(numeric, no check digit) | 3 = EAN8(numeric, 12 digits encoded) | 4 = EAN13(numeric, 12 digits encoded) | 5 = UPC � A(numeric, 12 digits encoded) | 6 = reserved for MONARCH | 7 =code 128 C w/o check digits(numeric only, even number printed) |8 =code 128 B w/o check digits(numeric) | 107 =code 128 C with check digits(numeric only, even number printed) | 108 =code 128 B with check digits(numeric)</param>
        /// <param name="barcodeWidthRatio">Bar width ratio: 0 = narrow bar = 1 dot, wide bar = 2 dots  | 1 = narrow bar = 1 dot, wide bar = 3 dots | 2 = narrow bar = 2 dots, wide bar = 5 dots</param>
        /// <param name="barcodeMultiplier"></param>
        /// <param name="barcodeHeight">Bar code height in dots</param>
        /// <param name="textUnder">Text under</param>
        /// <param name="barcodeData">pointer to barcode buffer</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNWriteBarCode", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNWriteBarcode(IntPtr handle, int printerType, int startX, int startY, int rotation, int barcodeType, int barcodeWidthRatio, int barcodeMultiplier, int barcodeHeight, int textUnder, byte[] barcodeData, out int errorCode);

        /// <summary>
        /// Downloads a firmware file to the printer
        /// </summary>
        /// <param name="handle">Printer driver handle</param>
        /// <param name="printerType">Printer type value</param>
        /// <param name="filename">Firmware file name (binary file)</param>
        /// <param name="errorCode">Pointer to returned error code</param>
        /// <returns>1 - successfull or  0 - failed</returns>
        [DllImport("ZBRPrinter.dll", EntryPoint = "ZBRPRNUpgradeFirmware", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int ZBRPRNUpgradeFirmware(IntPtr handle, int printerType, [MarshalAs(UnmanagedType.LPStr)]string filename, out int errorCode);

        #endregion Barcode Card Functions
    }
}